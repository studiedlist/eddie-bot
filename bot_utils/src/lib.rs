// 3 is more strict and fast, but might result in lowered values with shorter texts
// If you want comparison to be more precise with short sentences, use 2 instead
//
// Results with different sizes are shown in this module's tests
const DEFAULT_QGRAM_SIZE : usize = 3;

pub enum Similarity {
    High,
    Medium,
    Low,
}

/// Compares two slices of text and returns usize result from 0 to 100, where 0 is
/// totally different and 100 is totally equal
pub fn compare_sentences(a: &str, b: &str) -> usize {
    let qgram_size = DEFAULT_QGRAM_SIZE;
    qgram_comparison(a, b, qgram_size)
}

pub fn find_similar<'a>(
list: &Vec<&'a String>,
text: &str,
level: Similarity
) -> Option<Vec<&'a String>> {
    let text = format_text(text);
    if text.len() == 0 {
        return None
    }
    let level = similarity_value(&level);
    Some(
        list.into_iter()
            .filter(|elem| compare_sentences(elem, &text) > level)
            .fold(vec![], |mut result, elem| {
                result.push(elem);
                result
            })
        )
}

fn format_text(text: &str) -> String {
    let result = String::new();
    String::from(text.rsplit(" ")
        .filter(|elem| elem.len() > 2)
        .fold(result, |mut result, elem| {
            result.push_str(elem);
            result.push(' ');
            result
        }).trim())
}

fn qgram_comparison(a: &str, b: &str, qgram_size: usize) -> usize {
    let av = Vec::from(a);
    let bv = Vec::from(b);
    let bv_windows : Vec<_> = bv.windows(qgram_size).collect();
    let result = (0, 0, bv_windows.len());
    let result = av.windows(qgram_size).fold(result, |mut result, elem1| {
        if bv_windows.contains(&elem1) {
            result.0 += 1;
        }
        result.1 += 1;
        result
    });
    (2 * 100 * result.0)/(result.1 + result.2)
}

fn similarity_value(similarity: &Similarity) -> usize {
    match similarity {
        Similarity::High => 85,
        Similarity::Medium => 50,
        Similarity::Low => 20,
    }
}

#[cfg(test)]
mod test {

    use crate::text_analysis::qgram_comparison;

    #[test]
    fn compares_sentences() {
        assert!(100 == qgram_comparison("testtesttest", "testtesttest", 2));
        assert!(100 == qgram_comparison("test", "test", 2));
        assert!(40  == qgram_comparison("manipulacja", "mnipułcaja", 2));
        assert!(50  == qgram_comparison("testt", "textt", 2));

        assert!(100 == qgram_comparison("testtesttest", "testtesttest", 3));
        assert!(100 == qgram_comparison("test", "test", 3));
        assert!(22  == qgram_comparison("manipulacja", "mnipułcaja", 3));
        assert!(0   == qgram_comparison("testt", "textt", 3));
    }
}
