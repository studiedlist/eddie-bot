Simple messaging bot made after our dear friend Eddie  

# ToC
- [Features](#features)
  - [Rudelist](#rudelist)
  - [Blacklist](#blacklist)
  - [Substitution](#substitution)
  - [Replies](#replies)
  - [Expands](#expands)
  - [Man of the day](#man-of-the-day)
  - [Language switching](#language-switching)
- [Running bot](#running-bot)
- [Configuration](#configuration)
- [Roadmap](#roadmap)

# Features
- replying on messages that contains Eddie's name
- asking random question after some messages being skipped by bot
- answering to replies
- answering to voice messages 
- man of the day
- language switching
- rudelist
- blacklist
- expands and replies
- sed-like substitution
- persistent storage

## Rudelist
Rudelisted users are being offended by bot for a short defined period of time.
Rudelist is controlled by admins

These commands can be used in reply to target user's message:
- `/rudelist %time%` where time is number of minutes (default is 5 mins)
- `/unrudelist`

Rudelist is persistent and is set per group

## Blacklist
Blacklisted users are ignored by bot.  
In order to blacklist user, add his id to `blacklist.txt` file

## Substitution
Substitution syntax is similar to sed replace syntax.  
In order to use substitution, reply to target message with `s/%from%/%to%`,
where `%from%` is sequence you want to replace, and `%to%` is substitution
sequence

Additionally, you can pass `g` and `d` flags to the end of command
(like `s/ping/pong/gd`) to replace all occurrences or delete your command message
after substitution

Refer to [sedregex](https://gitlab.com/mexus/sedregex) for detailed syntax
description


## Replies
Reply is simple predefined message, which is sent by regex trigger
```json
"replies": [
  {
    "regex": "(?i)ping(\\s|$)",
    "reply": "pong"
  }
]
```
In above configuration example, if user sends message "ping", bot will reply with
message "pong"

### Reply variants
Multiple reply variants can be composed in order to make bot randomly select one
of options to reply with
```json
"replies": [
  {
    "regex": "(?i)hello(\\s|$)",
    "reply": [
      {
        "text": "Hi"
      },
      {
        "text": "Good morning",
        "chance": 0.5
      }
    ]
  }
]
```
In above configuration example bot will answer either "Hi" or "Good morning" to
messages containing "hello".  

### Chances
`chance` field defines probability of this variant to be selected by bot, where
0 is "will never be selected" and 1 is "will always be selected". Note that
`chance` field is optional- chance is set to 1 by default.  

In order to make bot actually select one of reply variants, you have to make
sure variants have chances set. You don't need to keep specific ordering of
variants by chance, because they will be automatically sorted in ascending
order.  

Replies themselves can also have chances set:
```json
"replies": [
  {
    "regex": "(?i)ping(\\s|$)",
    "reply": "pong",
    "chance": 0.5
  }
]
```
In above configuration example bot will answer roughly every second message
containing "pong"

### Message classes
By default, bot will reply to matched message regardless on its type. By setting
message class, you can make bot process given reply only if message type is
matched.
```json
"replies": [
  {
    "regex": "(?i)ping(\\s|$)",
    "reply": "pong",
    "message_class": "reply to bot"
  }
]
```
In above configuration example bot will reply to messages containing "ping", but
only those, which were send in reply to bot

Available message classes are:

- `reply to bot`
- `reply` (reply to any message)
- no value (any message)

### Media types
Bot can send multiple media types:

- voice
- picture
- video
- gif\*

\* Note that .mp4 with audio track (even empty) will be sent as video, not gif

```json
"replies": [
  {
    "regex": "(?i)Hello(\\s|$)",
    "reply": {
      "voice": "./hello.mp3",
    },
  },
  {
    "regex": "(?i)Cats(\\s|$)",
    "reply": [
      {
        "picture": "./funny_cats_1.png",
      },
      {
        "picture": "./funny_cats_2.png",
        "chance": 0.5
      }
    ]
  }
]
```

## Expands
Expand is substitution-like command, which is operating on messages that are
being matched by regex
```json
"expands": [
  {
    "regex" : "(?i)ping(\\s|$)",
    "from"  : "ping",
    "to"    : "pong",
    "global": true,
    "delete": true
  }
]
```
In above configuration example, if user sends message that contains "ping", all
occurrences of "ping" will be replaced with "pong" (as defined by "global"
attribute), and user's message will be deleted (as defined by "delete"
attribute)

You can use variables in `from` attribute in order to make expand depend on
message attributes

Available variables are:
- `bot.name`
- `user.first_name`
- `user.last_name`
- `username`
- `text`

You can also set `chance` field to expand as in reply

Check `config.json` to get more examples of expands and replies usage

## Man of the day
Once a day bot will randomly select one of members to be man of the day in reply
to `man_of_the_day` command.  
In order to have members to select from, bot collects and updates information
about users writing to chat (id, username, composed first & last name). This
data is stored for 48 hours, so, man of the day will always be selected among
active chat members.

## Language switching
Input data is marked with attributes (languages for now), and can be
filtered in order to make bot answer in some defined way

Example of using data markup is language switching 
Languages are stored as set, so you can not only switch to one language, but add
and remove several languages.
Note that sets of languages are persistent, and are set per group

You can use these command to switch bot languages:
- `/setl(ang) %lang%` 
- `/addl(ang) %lang%` 
- `/reml(ang) %lang%` 
- `/resetl(ang)` 

`%lang%` is language code used in markup


# Running bot
1. Clone repository
1. Install libssl-dev and sqlite (please open PR if any other dependencies are needed)
1. Set your tg bot API token: `export TELOXIDE_TOKEN=%YOUR_TOKEN%`
1. Run bot with `cargo r --release`

If you want to check config, pass `--check-config` argument (if you run bot with cargo, use `cargo r --release -- --check-config`)

# Configuration
If you want bot not to be Eddie but someone else, follow these steps:
1. Replace `input.json` with your data
2. Replace name in `config.yaml` with your bot's name  

[Message extracter](https://gitlab.com/studiedlist/tg_stat/-/tree/master/message_extracter)
utility can be used in order to prepare input data for bot


See [Features](#features) for configuration examples. Note that it is not necessary to write config exclusively in YAML since JSON is valid YAML

Other settings are related to filtering messages.  
You can change how similar messages are sent by changing Similarity values in
[handlers.rs](https://gitlab.com/studiedlist/eddie-bot/-/blob/master/src/handlers.rs) (available options are `Low`, `Medium` and `High`).
Furthermore, you can set up message comparison algorithm by changing DEFAULT_QGRAM_SIZE in [bot utils](https://gitlab.com/studiedlist/eddie-bot/-/blob/master/bot_utils/src/lib.rs).

# Roadmap
- Create example config
- Increase test coverage and compiler guarantees
- Simultaneous execution of message search and status sending
- Motd polls
- Automatic Telegram command menu configuration 
