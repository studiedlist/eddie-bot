use teloxide::prelude::*;

use std::error::Error;
use std::sync::Arc;

use repository::GroupRepository;
use typesafe_repository::{GetIdentity, Identity, IdentityBy};

pub mod repository;

pub struct SelectedGroup {
    pub name: String,
    pub chat_id: ChatId,
}

impl Identity for SelectedGroup {
    type Id = (String, ChatId);
}

impl GetIdentity for SelectedGroup {
    fn id(&self) -> Self::Id {
        (self.name.clone(), self.chat_id)
    }
}

impl IdentityBy<ChatId> for SelectedGroup {
    fn id_by(&self) -> ChatId {
        self.chat_id
    }
}

fn parse_group(msg: &Message) -> Option<String> {
    let text = msg.text()?;
    Some(String::from(text.split(' ').nth(1)?))
}

pub async fn set_uk(
    msg: Message,
    repo: Arc<dyn GroupRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let name = String::from("uk");
    let group = SelectedGroup {
        chat_id: msg.chat.id,
        name,
    };
    repo.remove_by(msg.chat.id).await?;
    repo.add(group).await?;
    Ok(())
}

pub async fn set(
    msg: Message,
    repo: Arc<dyn GroupRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    // TODO unable to parse group error message
    let Some(name) = parse_group(&msg) else { return Ok(()) };
    let group = SelectedGroup {
        chat_id: msg.chat.id,
        name,
    };
    repo.remove_by(msg.chat.id).await?;
    repo.add(group).await?;
    Ok(())
}

pub async fn add(
    msg: Message,
    repo: Arc<dyn GroupRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let Some(name) = parse_group(&msg) else { return Ok(()) };
    let group = SelectedGroup {
        chat_id: msg.chat.id,
        name,
    };
    repo.add(group).await?;

    Ok(())
}

pub async fn remove(
    msg: Message,
    repo: Arc<dyn GroupRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let Some(name) = parse_group(&msg) else { return Ok(()) };
    let group = (name, msg.chat.id);
    repo.remove(&group).await?;
    Ok(())
}

pub async fn reset(
    msg: Message,
    repo: Arc<dyn GroupRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    repo.remove_by(msg.chat.id).await?;
    Ok(())
}
