#![allow(clippy::module_name_repetitions)]

use super::SelectedGroup;
use anyhow::Error;
use async_trait::async_trait;
use rusqlite::params;
use teloxide::types::ChatId;
use tokio_rusqlite::Connection;
use typesafe_repository::prelude::*;

#[async_trait]
pub trait GroupRepository:
    Repository<SelectedGroup, Error = Error>
    + Add<SelectedGroup>
    + Remove<SelectedGroup>
    + ListBy<SelectedGroup, ChatId>
where
    Self: Send + Sync,
{
    async fn remove_by(&self, id: ChatId)
        -> Result<(), <Self as Repository<SelectedGroup>>::Error>;
}

pub struct SqliteGroupRepository {
    conn: Connection,
}

impl SqliteGroupRepository {
    pub async fn new(conn: Connection) -> Result<Self, <Self as Repository<SelectedGroup>>::Error> {
        conn.call(move |conn| {
            conn.execute(
                "CREATE TABLE IF NOT EXISTS selected_groups (name TEXT, chat_id INTEGER, PRIMARY KEY (name, chat_id))",
                [],
            )
        }).await?;
        Ok(Self { conn })
    }
}

impl Repository<SelectedGroup> for SqliteGroupRepository {
    type Error = Error;
}

#[async_trait]
impl Add<SelectedGroup> for SqliteGroupRepository {
    async fn add(&self, g: SelectedGroup) -> Result<(), Self::Error> {
        self
            .conn
            .call(move |conn| {
                conn.execute(
                    "INSERT INTO selected_groups (name, chat_id) VALUES (?1, ?2) ON CONFLICT (name, chat_id) DO NOTHING;",
                    params![
                        g.name,
                        g.chat_id.0,
                    ]
                )
            }).await?;
        Ok(())
    }
}

#[async_trait]
impl Remove<SelectedGroup> for SqliteGroupRepository {
    async fn remove(
        &self,
        id: &IdentityOf<SelectedGroup>,
    ) -> Result<Option<SelectedGroup>, Self::Error> {
        let id = id.clone();
        self.conn
            .call(move |conn| {
                conn.execute(
                    "DELETE FROM selected_groups WHERE name = ?1 AND chat_id = ?2",
                    params![id.0, id.1 .0,],
                )
            })
            .await?;
        Ok(None)
    }
}

#[async_trait]
impl ListBy<SelectedGroup, ChatId> for SqliteGroupRepository {
    async fn list_by(&self, id: &ChatId) -> Result<Vec<SelectedGroup>, Self::Error> {
        let id = *id;
        let names = self
            .conn
            .call(move |conn| {
                let mut stmt =
                    conn.prepare("SELECT name FROM selected_groups WHERE chat_id = ?1")?;
                let names = stmt
                    .query_map([id.0], |row| row.get(0))?
                    .filter_map(Result::ok)
                    .collect::<Vec<String>>();
                Ok::<_, rusqlite::Error>(names)
            })
            .await?;
        let names = names
            .into_iter()
            .map(|name| SelectedGroup { chat_id: id, name })
            .collect();
        Ok(names)
    }
}

#[async_trait]
impl GroupRepository for SqliteGroupRepository {
    async fn remove_by(
        &self,
        id: ChatId,
    ) -> Result<(), <Self as Repository<SelectedGroup>>::Error> {
        self.conn
            .call(move |conn| {
                conn.execute(
                    "DELETE FROM selected_groups WHERE chat_id = ?1",
                    params![id.0],
                )
            })
            .await?;
        Ok(())
    }
}
