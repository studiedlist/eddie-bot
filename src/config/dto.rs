#![allow(clippy::from_over_into)]

use super::MessageClass;
use regex::Regex;
use serde::Deserialize;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::ops::RangeInclusive;
use std::path::PathBuf;

const CHANCE_RANGE: RangeInclusive<f32> = 0.0..=1.0;

#[derive(Deserialize)]
pub struct Config {
    pub name: String,
    pub typing_speed: u16,
    pub question_delay: u16,
    pub paths: PathConfig,
    pub regex: RegexConfig,
    pub db: DatabaseConfig,
    pub messages: MessageConfig,
    pub expands: Vec<Expand>,
    pub replies: Vec<Reply>,
    pub commands: HashMap<String, Command>,
    pub counters: Vec<Counter>,
}

#[derive(Deserialize)]
pub struct MessageConfig {
    motd_not_enough_members: String,
    motd_rolling_started: String,
    motd_rolling_success: String,
    motd_exists: String,
    motd_stat: String,
    motd_stat_empty: String,
    members_list_message: String,
    members_list_empty: String,
}

impl Into<super::MessageConfig> for MessageConfig {
    fn into(self) -> super::MessageConfig {
        let MessageConfig {
            motd_not_enough_members,
            motd_rolling_started,
            motd_rolling_success,
            motd_exists,
            members_list_message,
            members_list_empty,
            motd_stat,
            motd_stat_empty,
        } = self;
        super::MessageConfig {
            motd_not_enough_members,
            motd_rolling_started,
            motd_rolling_success,
            motd_exists,
            motd_stat_empty,
            motd_stat,
            members_list_message,
            members_list_empty,
        }
    }
}

#[derive(Deserialize)]
pub struct PathConfig {
    pub input: PathBuf,
    pub blacklist: PathBuf,
}

impl Into<super::PathConfig> for PathConfig {
    fn into(self) -> super::PathConfig {
        let PathConfig { input, blacklist } = self;
        super::PathConfig { input, blacklist }
    }
}

#[derive(Deserialize)]
pub struct RegexConfig {
    pub answer: String,
    pub question: String,
    pub fuck_you: String,
    pub rude_answer: String,
    pub kick: Vec<String>,
    pub substitution: String,
}

impl TryInto<super::RegexConfig> for RegexConfig {
    type Error = anyhow::Error;

    fn try_into(self) -> Result<super::RegexConfig, Self::Error> {
        let RegexConfig {
            answer,
            question,
            fuck_you,
            rude_answer,
            kick,
            substitution,
        } = self;
        let kick = kick
            .iter()
            .map(|kick| Regex::new(kick))
            .collect::<Result<_, _>>()?;
        Ok(super::RegexConfig {
            kick,
            answer: Regex::new(&answer)?,
            question: Regex::new(&question)?,
            fuck_you: Regex::new(&fuck_you)?,
            rude_answer: Regex::new(&rude_answer)?,
            substitution: Regex::new(&substitution)?,
        })
    }
}

#[derive(Deserialize, Hash, PartialEq, Eq)]
pub struct Command {
    pub regex: String,
    pub args: Option<Vec<Argument>>,
}

impl Command {
    /// Generates regex from command
    /// ```
    /// use eddie_bot::config::dto::{Command, ArgumentType, Argument};
    /// use regex::Regex;
    ///
    /// let cmd = Command {
    ///     regex: String::from("ex(ample)"),
    ///     args: Some(vec![Argument { arg_type: ArgumentType::Integer, len: 3 }]),
    /// };
    /// let regex = cmd.to_regex().unwrap();
    /// assert_eq!(r"^/ex(ample)?\s(\d{1,3})", regex.as_str());
    ///
    /// assert!(!regex.is_match("/ex"));
    /// assert!(regex.is_match("/ex 12"));
    /// assert!(!regex.is_match("ex 12"));
    /// assert!(regex.is_match("/example 123"));
    ///
    /// let cmd = Command {
    ///     regex: String::from("ex(ample)"),
    ///     args: None,
    /// };
    /// let regex = cmd.to_regex().unwrap();
    /// assert_eq!(r"^/ex(ample)?", regex.as_str());
    ///
    /// assert!(regex.is_match("/ex"));
    /// assert!(regex.is_match("/example"));
    /// assert!(!regex.is_match("example"));
    /// ```
    pub fn to_regex(&self) -> Result<Regex, anyhow::Error> {
        const PATTERN_SEPARATOR: char = '|';
        let arg_pattern = if let Some(args) = &self.args {
            let mut arg_pattern = String::new();
            for (i, arg) in args.iter().enumerate() {
                arg_pattern.push_str(&arg.to_pattern());
                if i < args.len().wrapping_sub(1) {
                    arg_pattern.push(PATTERN_SEPARATOR);
                }
            }
            format!(r"\s({arg_pattern})")
        } else {
            String::new()
        };
        let cmd = &self.regex.replace(')', ")?");
        let pattern = super::commands::COMMAND_PATTERN
            .replace(super::commands::CMD_PLACEHOLDER, cmd)
            .replace(super::commands::ARGS_PLACEHOLDER, &arg_pattern);
        Ok(Regex::new(&pattern)?)
    }
}

#[derive(Deserialize, Hash, PartialEq, Eq)]
pub struct Argument {
    pub arg_type: ArgumentType,
    pub len: u8,
}

impl Argument {
    pub fn to_pattern(&self) -> String {
        let mut pattern = self.arg_type.to_pattern();
        pattern.push_str(&format!("{{1,{}}}", self.len));
        pattern
    }
}

#[derive(Deserialize, Hash, PartialEq, Eq)]
pub enum ArgumentType {
    String,
    Integer,
}

impl ArgumentType {
    pub fn to_pattern(&self) -> String {
        match self {
            ArgumentType::String => String::from("[a-z]"),
            ArgumentType::Integer => String::from(r"\d"),
        }
    }
}

#[derive(Deserialize)]
pub struct DatabaseConfig {
    pub path: PathBuf,
}

impl Into<super::DatabaseConfig> for DatabaseConfig {
    fn into(self) -> super::DatabaseConfig {
        super::DatabaseConfig { path: self.path }
    }
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum ReplyRegex {
    Text(String),
    List(Vec<String>),
}

#[derive(Deserialize)]
pub struct Reply {
    pub regex: ReplyRegex,
    pub reply: ReplyVariant,
    pub chance: Option<f32>,
    pub msg_class: Option<String>,
}

impl TryInto<super::Reply> for Reply {
    type Error = anyhow::Error;

    fn try_into(self) -> Result<super::Reply, Self::Error> {
        let Reply {
            regex,
            reply,
            chance,
            msg_class,
        } = self;
        let regex: super::ReplyRegex = match regex {
            ReplyRegex::Text(s) => super::ReplyRegex::Text(Regex::new(&s)?),
            ReplyRegex::List(l) => {
                let l: Vec<_> = l
                    .into_iter()
                    .map(|r| Regex::new(&r))
                    .collect::<Result<_, _>>()?;
                super::ReplyRegex::List(l)
            }
        };
        if chance.map_or(false, |c| !CHANCE_RANGE.contains(&c)) {
            return Err(anyhow::anyhow!(
                "Invalid chance for reply {:?}\nChance must be between {} and {}",
                regex,
                CHANCE_RANGE.start(),
                CHANCE_RANGE.end(),
            ));
        }
        let msg_class = msg_class
            .map(|name| {
                MessageClass::from(&name).ok_or(anyhow::anyhow!("Unknown message class {name}"))
            })
            .transpose()?;
        let reply = reply.into();
        Ok(super::Reply {
            regex,
            reply,
            chance,
            msg_class,
        })
    }
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum ReplyVariant {
    Text(String),
    Video { video: String },
    Gif { gif: String },
    Picture { picture: String },
    Voice { voice: String },
    Texts(Vec<ReplyText>),
}

impl Into<super::ReplyVariant> for ReplyVariant {
    fn into(self) -> super::ReplyVariant {
        use ReplyVariant::{Gif, Picture, Text, Texts, Video, Voice};
        match self {
            Text(t) => super::ReplyVariant::Text(t),
            Video { video } => super::ReplyVariant::Video(PathBuf::from(video)),
            Gif { gif } => super::ReplyVariant::Gif(PathBuf::from(gif)),
            Picture { picture } => super::ReplyVariant::Picture(PathBuf::from(picture)),
            Voice { voice } => super::ReplyVariant::Voice(PathBuf::from(voice)),
            Texts(v) => {
                let mut v: Vec<_> = v
                    .into_iter()
                    .map(|t| {
                        let chance = t.chance();
                        let t = t.into();
                        (t, chance)
                    })
                    .collect();
                v.sort_by(|a, b| {
                    let (a, b) = (a.1.unwrap_or(1.0), b.1.unwrap_or(1.0));
                    a.partial_cmp(&b).unwrap_or(Ordering::Equal)
                });
                super::ReplyVariant::Texts(v)
            }
        }
    }
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum ReplyText {
    Text {
        text: String,
        chance: Option<f32>,
    },
    Video {
        video: String,
        chance: Option<f32>,
    },
    Gif {
        gif: String,
        chance: Option<f32>,
    },
    Picture {
        picture: String,
        chance: Option<f32>,
    },
    Voice {
        voice: String,
        chance: Option<f32>,
    },
}

impl ReplyText {
    pub fn chance(&self) -> Option<f32> {
        match self {
            Self::Text { chance, .. }
            | Self::Video { chance, .. }
            | Self::Gif { chance, .. }
            | Self::Picture { chance, .. }
            | Self::Voice { chance, .. } => *chance,
        }
    }
}

impl Into<super::ReplyText> for ReplyText {
    fn into(self) -> super::ReplyText {
        use ReplyText::{Gif, Picture, Text, Video, Voice};
        match self {
            Text { text, .. } => super::ReplyText::Text(text),
            Video { video, .. } => super::ReplyText::Video(PathBuf::from(video)),
            Gif { gif, .. } => super::ReplyText::Gif(PathBuf::from(gif)),
            Picture { picture, .. } => super::ReplyText::Picture(PathBuf::from(picture)),
            Voice { voice, .. } => super::ReplyText::Voice(PathBuf::from(voice)),
        }
    }
}

#[derive(Deserialize)]
pub struct Expand {
    pub regex: String,
    pub from: String,
    pub to: String,
    pub global: bool,
    pub delete: bool,
    pub chance: Option<f32>,
    pub msg_class: Option<String>,
}

impl TryInto<super::Expand> for Expand {
    type Error = anyhow::Error;

    fn try_into(self) -> Result<super::Expand, Self::Error> {
        let Expand {
            regex,
            from,
            to,
            global,
            delete,
            chance,
            msg_class,
        } = self;
        let msg_class = msg_class
            .map(|name| {
                MessageClass::from(&name).ok_or(anyhow::anyhow!("Unknown message class {name}"))
            })
            .transpose()?;
        Ok(super::Expand {
            regex: Regex::new(&regex)?,
            from: Regex::new(&from)?,
            to,
            global,
            delete,
            chance: chance.unwrap_or(1.0),
            msg_class,
        })
    }
}

#[derive(Deserialize)]
pub struct Counter {
    pub name: String,
    pub regex: HashMap<CounterEvent, String>,
    pub messages: HashMap<CounterEvent, String>,
    #[serde(rename = "type")]
    pub counter_type: CounterType,
}

impl TryInto<super::Counter> for Counter {
    type Error = anyhow::Error;

    fn try_into(self) -> Result<super::Counter, Self::Error> {
        let Counter {
            name,
            regex,
            messages,
            counter_type,
        } = self;
        let counter_type = counter_type.into();
        let messages = messages.into_iter().map(|(t, m)| (t.into(), m)).collect();
        let regex: Result<Vec<_>, anyhow::Error> = regex
            .into_iter()
            .map(|(t, r)| Ok((t, Regex::new(&r)?)))
            .collect();
        let regex = regex?.into_iter().map(|(t, r)| (t.into(), r)).collect();
        Ok(super::Counter {
            name,
            regex,
            messages,
            counter_type,
        })
    }
}

#[derive(PartialEq, Eq, Hash, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum CounterEvent {
    Increase,
    Decrease,
}

impl Into<super::counters::CounterEvent> for CounterEvent {
    fn into(self) -> super::counters::CounterEvent {
        match self {
            CounterEvent::Increase => super::counters::CounterEvent::Increase,
            CounterEvent::Decrease => super::counters::CounterEvent::Decrease,
        }
    }
}

#[derive(Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum CounterType {
    #[serde(rename = "reply to user")]
    ReplyToUser,
    Any,
}

impl Into<super::counters::CounterType> for CounterType {
    fn into(self) -> super::counters::CounterType {
        match self {
            CounterType::ReplyToUser => super::counters::CounterType::ReplyToUser,
            CounterType::Any => super::counters::CounterType::Any,
        }
    }
}
