use crate::config;
use crate::config::dto::{Config, Reply};
use regex::Regex;
use std::cmp::Ordering;
use std::error::Error;
use std::path::Path;

pub fn parse_config(path: &Path) -> Result<config::Config, Box<dyn Error>> {
    let content = std::fs::read_to_string(path)?;
    let mut config: Config = serde_yaml::from_str(&content)?;
    config.regex.answer = config.regex.answer.replacen("%bot.name%", &config.name, 1);
    let mut commands: Vec<(_, Regex)> = config
        .commands
        .iter()
        .map(|(name, c)| match config::Command::from_name(name) {
            Some(command) => Ok((command, c.to_regex()?)),
            None => Err(anyhow::anyhow!("Invalid command '{name}'")),
        })
        .collect::<Result<_, _>>()?;
    commands.sort_by(|(_, a), (_, b)| b.as_str().len().cmp(&a.as_str().len()));
    let Config {
        name,
        typing_speed,
        question_delay,
        expands,
        mut replies,
        messages,
        counters,
        ..
    } = config;
    let expands = expands
        .into_iter()
        .map(TryInto::try_into)
        .collect::<Result<_, _>>()?;
    replies
        .sort_by(|a: &Reply, b: &Reply| a.chance.partial_cmp(&b.chance).unwrap_or(Ordering::Equal));
    let replies: Vec<_> = replies
        .into_iter()
        .rev()
        .map(TryInto::try_into)
        .collect::<Result<_, _>>()?;
    let counters: Vec<_> = counters
        .into_iter()
        .map(TryInto::try_into)
        .collect::<Result<_, _>>()?;
    let config = config::Config {
        name,
        typing_speed,
        question_delay,
        paths: config.paths.into(),
        regex: config.regex.try_into()?,
        db: config.db.into(),
        messages: messages.into(),
        expands,
        replies,
        commands,
        counters,
    };
    Ok(config)
}
