#![allow(clippy::module_name_repetitions)]
pub use commands::Command;
pub use counters::Counter;
pub use expands::Expand;
use regex::Regex;
use std::path::PathBuf;
use teloxide::types::Message;

pub use yaml::parse_config as parse_config_yaml;

pub mod dto;
pub mod yaml;

pub mod commands;
pub mod counters;
pub mod expands;

pub struct Config {
    pub name: String,
    pub typing_speed: u16,
    pub question_delay: u16,
    pub paths: PathConfig,
    pub regex: RegexConfig,
    pub db: DatabaseConfig,
    pub expands: Vec<Expand>,
    pub replies: Vec<Reply>,
    pub commands: Vec<(Command, Regex)>,
    pub messages: MessageConfig,
    pub counters: Vec<Counter>,
}

impl Config {
    pub fn command(&self, command: &Command) -> Option<&Regex> {
        self.commands
            .iter()
            .find(|(c, _)| command == c)
            .map(|(_, r)| r)
    }
}

pub struct MessageConfig {
    pub motd_not_enough_members: String,
    pub motd_rolling_started: String,
    pub motd_rolling_success: String,
    pub motd_exists: String,
    pub motd_stat_empty: String,
    pub motd_stat: String,
    pub members_list_message: String,
    pub members_list_empty: String,
}

pub struct PathConfig {
    pub input: PathBuf,
    pub blacklist: PathBuf,
}

pub struct RegexConfig {
    pub answer: Regex,
    pub question: Regex,
    pub fuck_you: Regex,
    pub rude_answer: Regex,
    pub kick: Vec<Regex>,
    pub substitution: Regex,
}

pub struct DatabaseConfig {
    pub path: PathBuf,
}

#[derive(Debug)]
pub enum ReplyRegex {
    Text(Regex),
    List(Vec<Regex>),
}

#[derive(Debug)]
pub struct Reply {
    pub regex: ReplyRegex,
    pub reply: ReplyVariant,
    pub chance: Option<f32>,
    pub msg_class: Option<MessageClass>,
}

#[derive(Debug)]
pub enum ReplyVariant {
    Text(String),
    Video(PathBuf),
    Gif(PathBuf),
    Picture(PathBuf),
    Voice(PathBuf),
    Texts(Vec<(ReplyText, Option<f32>)>),
}

#[derive(Debug)]
pub enum ReplyText {
    Text(String),
    Video(PathBuf),
    Gif(PathBuf),
    Picture(PathBuf),
    Voice(PathBuf),
}

#[derive(Debug)]
pub enum MessageClass {
    ReplyToBot,
    Reply,
}

impl MessageClass {
    pub fn from(name: &str) -> Option<MessageClass> {
        match name.trim().to_lowercase().as_str() {
            "reply" => Some(MessageClass::Reply),
            "reply to bot" => Some(MessageClass::ReplyToBot),
            _ => None,
        }
    }
    pub fn matches(&self, msg: &Message, bot_id: u64) -> bool {
        match self {
            MessageClass::ReplyToBot => msg.reply_to_message().map_or(false, |msg| {
                let id = match msg.from() {
                    Some(u) => u.id,
                    None => return false,
                };
                id.0 == bot_id
            }),
            MessageClass::Reply => msg.reply_to_message().is_some(),
        }
    }
}
