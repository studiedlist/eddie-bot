use regex::Regex;
use std::collections::HashMap;
use teloxide::types::Message;

#[derive(Debug, Clone)]
pub struct Counter {
    pub name: String,
    pub regex: Vec<(CounterEvent, Regex)>,
    pub messages: HashMap<CounterEvent, String>,
    pub counter_type: CounterType,
}

impl Counter {
    pub fn format(&self, event: &CounterEvent, args: &[(FormatArg, String)]) -> Option<String> {
        let msg = self.messages.iter().find(|(e, _)| *e == event)?.1.clone();
        Some(
            args.iter()
                .fold(msg, |res, (arg, text)| res.replace(arg.name(), text)),
        )
    }
    pub fn format_with_message(
        &self,
        msg: &Message,
        event: &CounterEvent,
        count: i64,
    ) -> Option<String> {
        use FormatArg::{Count, Target, User};
        let format_user = |user: &teloxide::types::User| {
            user.last_name.clone().map_or_else(
                || user.first_name.clone(),
                |last_name| format!("{} {last_name}", user.first_name),
            )
        };
        let user = msg.from()?;
        let username = format_user(user);
        let args = match &self.counter_type {
            CounterType::ReplyToUser => {
                let target = msg.reply_to_message()?.from()?;
                let target = format_user(target);
                vec![
                    (User, username),
                    (Target, target),
                    (Count, format!("{count}")),
                ]
            }
            CounterType::Any => vec![(User, username), (Count, format!("{count}"))],
        };
        self.format(event, &args)
    }
    pub fn matches_type(&self, msg: &Message) -> bool {
        match &self.counter_type {
            CounterType::ReplyToUser => {
                msg.reply_to_message().map_or(false, |r| r.from().is_some())
            }
            CounterType::Any => true,
        }
    }
}

pub enum FormatArg {
    Target,
    User,
    Count,
}

impl FormatArg {
    pub fn name(&self) -> &'static str {
        match self {
            FormatArg::Target => "%target%",
            FormatArg::Count => "%count%",
            FormatArg::User => "%user%",
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum CounterEvent {
    Increase,
    Decrease,
}

#[derive(Debug, Clone)]
pub enum CounterType {
    ReplyToUser,
    Any,
}
