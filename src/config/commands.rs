use lazy_static::lazy_static;

pub static CMD_PLACEHOLDER: &str = "%cmd%";
pub static ARGS_PLACEHOLDER: &str = "%args%";

lazy_static! {
    pub static ref COMMAND_PATTERN: String = format!(r"^/{CMD_PLACEHOLDER}{ARGS_PLACEHOLDER}");
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
pub enum Command {
    Rudelist,
    Unrudelist,
    SetGroup,
    SetGroupUk,
    AddGroup,
    RemoveGroup,
    ResetGroups,
    AvailableGroups,
    CurrentGroups,
    ManOfTheDay,
    ManOfTheDayStat,
    ListMembers,
}

impl Command {
    pub fn from_name(s: &str) -> Option<Command> {
        Some(match s {
            "rudelist" => Command::Rudelist,
            "set_group" => Command::SetGroup,
            "add_group" => Command::AddGroup,
            "unrudelist" => Command::Unrudelist,
            "set_group_uk" => Command::SetGroupUk,
            "available_groups" => Command::AvailableGroups,
            "current_groups" => Command::CurrentGroups,
            "remove_group" => Command::RemoveGroup,
            "reset_groups" => Command::ResetGroups,
            "man_of_the_day" => Command::ManOfTheDay,
            "list_members" => Command::ListMembers,
            "man_of_the_day_stat" => Command::ManOfTheDayStat,
            _ => return None,
        })
    }
}
