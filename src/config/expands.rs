use crate::config::Config;
use lazy_regex::regex;
use regex::Regex;
use std::sync::Arc;
use teloxide::types::Message;

#[derive(Debug)]
pub struct Expand {
    pub regex: Regex,
    pub from: Regex,
    pub to: String,
    pub global: bool,
    pub delete: bool,
    pub chance: f32,
    pub msg_class: Option<super::MessageClass>,
}

impl Expand {
    pub fn replace(&self, text: &str, msg: &Message, config: &Arc<Config>) -> Option<String> {
        let vars = Variable::parse(&self.to);
        let mut to = self.to.clone();
        for var in vars {
            let replace_to = match var {
                Variable::BotName => config.name.clone(),
                Variable::Username => msg.from()?.username.as_ref()?.clone(),
                Variable::UserFirstName => msg.from()?.first_name.clone(),
                Variable::UserLastName => msg
                    .from()?
                    .last_name
                    .as_ref()
                    .unwrap_or(&String::new())
                    .clone(),
                Variable::Text => String::from(msg.text()?),
            };
            let name = format!("%{}%", &var.name());
            to = to.replace(&name, &replace_to);
        }
        let Expand { from, global, .. } = self;
        let text = match global {
            true => from.replace_all(text, &to),
            false => from.replace(text, &to),
        };
        Some(text.replace("  ", " "))
    }
}

#[derive(Debug)]
pub enum Variable {
    BotName,
    UserFirstName,
    UserLastName,
    Username,
    Text,
}

impl Variable {
    pub fn parse(text: &str) -> Vec<Variable> {
        regex!(r"%[^%]*%")
            .find_iter(text)
            .filter_map(|c| Variable::by_name(&c.as_str().replace('%', "")))
            .collect()
    }

    pub fn by_name(name: &str) -> Option<Variable> {
        match name {
            "bot.name" => Some(Variable::BotName),
            "user.first_name" => Some(Variable::UserFirstName),
            "user.last_name" => Some(Variable::UserLastName),
            "username" => Some(Variable::Username),
            "text" => Some(Variable::Text),
            _ => None,
        }
    }

    pub fn name(&self) -> String {
        String::from(match self {
            Variable::BotName => "bot.name",
            Variable::UserFirstName => "user.first_name",
            Variable::UserLastName => "user.last_name",
            Variable::Username => "username",
            Variable::Text => "text",
        })
    }
}
