use eddie_bot::config::*;
use eddie_bot::filters;
use eddie_bot::groups;
use eddie_bot::handlers;
use eddie_bot::motd;
use eddie_bot::rudelist;
use teloxide::prelude::*;

use std::path::PathBuf;
use std::sync::Arc;

use dptree::{filter, filter_async, filter_map};

#[tokio::main]
async fn main() {

    let t = chrono::Utc::now();
    let config =
        parse_config_yaml(&PathBuf::from("config.yaml")).expect("Cannot parse config file");
    let args: Vec<String> = std::env::args().collect();
    if args.iter().any(|a| a == "--check-config") {
        let time_now = chrono::Utc::now();
        let delta = time_now.timestamp_millis() - t.timestamp_millis();
        println!("Config is correct");
        println!("Parsed in {delta} ms");
        return;
    }
    pretty_env_logger::init();
    log::info!("Starting");

    let blacklist_path = &config.paths.blacklist;
    let input_path = &config.paths.input;

    let blacklist = match eddie_bot::parse_blacklist(blacklist_path) {
        Ok(b) => b,
        Err(err) => {
            log::warn!("Error parsing blacklist:\n{}", err);
            vec![]
        },
    };

    let bot = Bot::from_env();
    let id = bot
        .get_me()
        .send()
        .await
        .expect("Error getting bot id")
        .user
        .id
        .0;
    let t = chrono::Utc::now();
    log::info!("{t:?}");
    log::info!("Startup finished");
    log::info!("Id is {id}");
    let mut container = DependencyMap::new();
    let msgs = eddie_bot::parse_messages(input_path).unwrap();

    use eddie_bot::counters::repository::{CounterValueRepository, SqliteCounterValueRepository};
    use eddie_bot::groups::repository::{GroupRepository, SqliteGroupRepository};
    use eddie_bot::member::repository::{MemberRepository, SqliteMemberRepository};
    use eddie_bot::motd::repository::{MotdRepository, SqliteMotdRepository};
    use eddie_bot::rudelist::repository::{RudelistedRepository, SqliteRudelistedRepository};
    use tokio_rusqlite::Connection;

    let connection = Connection::open(&config.db.path).await.unwrap();

    let motd_repository: Arc<dyn MotdRepository> =
        Arc::new(SqliteMotdRepository::new(connection.clone()).await.unwrap());
    let member_repository: Arc<dyn MemberRepository> = Arc::new(
        SqliteMemberRepository::new(connection.clone(), bot.clone())
            .await
            .unwrap(),
    );
    let rudelisted_repository: Arc<dyn RudelistedRepository> = Arc::new(
        SqliteRudelistedRepository::new(connection.clone())
            .await
            .unwrap(),
    );
    let counter_repository: Arc<dyn CounterValueRepository> = Arc::new(
        SqliteCounterValueRepository::new(connection.clone())
            .await
            .unwrap(),
    );
    let group_repository: Arc<dyn GroupRepository> = Arc::new(
        SqliteGroupRepository::new(connection.clone())
            .await
            .unwrap(),
    );

    container.insert(msgs);
    container.insert(blacklist);
    container.insert(id);
    container.insert(Arc::new(config));
    container.insert(motd_repository);
    container.insert(member_repository);
    container.insert(rudelisted_repository);
    container.insert(counter_repository);
    container.insert(group_repository);

    let filter_cmd = |command: Command| filter(move |cmd: Arc<Command>| *cmd == command);

    let handler = Update::filter_message().branch(
        filter(filters::not_blacklisted)
            .branch(filter(filters::matches_substitution).endpoint(handlers::substitution))
            .chain(filter_async(filters::process_member))
            .chain(filter(filters::not_forwarded))
            .chain(filter(filters::not_outdated))
            .branch(filter_map(filters::matches_counter).endpoint(handlers::counter))
            .branch(
                filter_map(filters::is_command)
                    .branch(
                        filter_async(eddie_bot::checks::is_admin)
                            .branch(
                                filter(filters::reply_not_to_bot)
                                    .branch(
                                        filter_cmd(Command::Unrudelist)
                                            .endpoint(rudelist::handlers::unrudelist),
                                    )
                                    .branch(
                                        filter_cmd(Command::Rudelist)
                                            .endpoint(rudelist::handlers::rudelist),
                                    ),
                            )
                            .branch(
                                filter(filters::reply_to_bot)
                                    .branch(
                                        filter_cmd(Command::SetGroupUk)
                                            .endpoint(groups::set_uk),
                                    )
                                    .branch(
                                        filter_cmd(Command::SetGroup).endpoint(groups::set),
                                    )
                                    .branch(
                                        filter_cmd(Command::AddGroup).endpoint(groups::add),
                                    )
                                    .branch(
                                        filter_cmd(Command::ResetGroups)
                                            .endpoint(groups::reset),
                                    )
                                    .branch(
                                        filter_cmd(Command::RemoveGroup)
                                            .endpoint(groups::remove),
                                    ),
                            ),
                    )
                    .branch(
                        filter_cmd(Command::ManOfTheDay).endpoint(motd::handlers::man_of_the_day),
                    )
                    .branch(
                        filter_cmd(Command::ManOfTheDayStat)
                            .endpoint(motd::handlers::man_of_the_day_stat),
                    )
                    .branch(filter_cmd(Command::ListMembers).endpoint(handlers::list_members)),
            )
            .branch(filter(filters::kick_trigger).endpoint(handlers::rude_answer))
            .branch(filter_async(filters::is_rudelisted).endpoint(handlers::rude_answer))
            .branch(
                filter(|msg: Message| msg.text().is_some())
                    .branch(filter(filters::matches_expand).endpoint(handlers::expand))
                    .branch(filter(filters::matches_reply).endpoint(handlers::reply))
                    .branch(filter(filters::matches_substitution).endpoint(handlers::substitution)),
            )
            .branch(filter(filters::fuck_you).endpoint(handlers::rude_answer))
            .branch(filter(filters::reply_to_bot).endpoint(handlers::message_reply))
            .branch(filter(filters::answer).endpoint(handlers::answer))
            .branch(filter(filters::question_counter).endpoint(handlers::question)),
    );
    Dispatcher::builder(bot, handler)
        .dependencies(container)
        .enable_ctrlc_handler()
        .default_handler(|upd| async move {
            log::debug!("Unhandled update: {:?}", upd);
        })
        .build()
        .dispatch()
        .await;
}
