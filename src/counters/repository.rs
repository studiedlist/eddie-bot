#![allow(clippy::module_name_repetitions)]

use super::CounterValue;
use async_trait::async_trait;
use typesafe_repository::prelude::*;

#[async_trait]
pub trait CounterValueRepository:
    Repository<CounterValue, Error = anyhow::Error>
    + Get<CounterValue>
    + Save<CounterValue>
    + Send
    + Sync
{
    async fn get_or_create(
        &self,
        id: &IdentityOf<CounterValue>,
    ) -> Result<CounterValue, anyhow::Error> {
        let value = self.get_one(id).await?;
        if let Some(value) = value {
            Ok(value)
        } else {
            let c = CounterValue::from_id(id.clone(), CounterValue::default_value());
            self.save(c.clone()).await?;
            Ok(c)
        }
    }

    async fn decrease(&self, id: &IdentityOf<CounterValue>) -> Result<i64, anyhow::Error> {
        let mut value = self.get_or_create(id).await?;
        value.value = if let Some(v) = value.value.checked_sub(1) {
            v
        } else {
            log::warn!("Error decreasing counter: counter value is already minimal");
            value.value
        };
        let val = value.value;
        self.save(value).await?;
        Ok(val)
    }

    async fn increase(&self, id: &IdentityOf<CounterValue>) -> Result<i64, anyhow::Error> {
        let mut value = self.get_or_create(id).await?;
        value.value = if let Some(v) = value.value.checked_add(1) {
            v
        } else {
            log::warn!("Error increasing counter: counter value is already maximum");
            value.value
        };
        let val = value.value;
        self.save(value).await?;
        Ok(val)
    }
}

use rusqlite::params;
use tokio_rusqlite::Connection;

pub struct SqliteCounterValueRepository {
    pub conn: Connection,
}

impl SqliteCounterValueRepository {
    pub async fn new(conn: Connection) -> Result<Self, anyhow::Error> {
        conn.call(move |conn| {
            conn.execute(
                "CREATE TABLE IF NOT EXISTS counters (name STRING, chat_id INTEGER, user_id INTEGER, username STRING, member_name STRING, count INTEGER, target INTEGER, PRIMARY KEY (user_id, chat_id, name, target));", 
                [],
            )
        }).await?;
        Ok(Self { conn })
    }
}

impl Repository<CounterValue> for SqliteCounterValueRepository {
    type Error = anyhow::Error;
}

#[async_trait]
impl Get<CounterValue> for SqliteCounterValueRepository {
    async fn get_one(
        &self,
        id: &IdentityOf<CounterValue>,
    ) -> Result<Option<CounterValue>, anyhow::Error> {
        let (counter, user, target, chat) = id.clone();
        let name = counter.name.clone();
        let value : Option<i64> = self.conn.call(move |conn| {
            let mut stmt = conn
                .prepare("SELECT count FROM counters WHERE chat_id = ?1 AND user_id = ?2 AND name = ?3 AND target = ?4")?;
            let mut iter = stmt.query_map(
                rusqlite::params![chat.0, user.0, name, target.0],
                |row| row.get(0),
            )?;
            match iter.next() {
                Some(c) => Ok::<_, anyhow::Error>(c?),
                None => Ok(None),
            }
        }).await?;
        if let Some(value) = value {
            Ok(Some(CounterValue {
                value,
                target,
                chat,
                user,
                counter,
            }))
        } else {
            Ok(None)
        }
    }
}

#[async_trait]
impl Save<CounterValue> for SqliteCounterValueRepository {
    async fn save(&self, v: CounterValue) -> Result<(), anyhow::Error> {
        self.conn
            .call(move |conn| {
                conn.execute(
                    "INSERT INTO counters (user_id, chat_id, name, count, target)
                      VALUES(?1, ?2, ?3, ?4, ?5) 
                      ON CONFLICT(user_id, chat_id, name, target) 
                      DO UPDATE SET count=excluded.count;",
                    params![v.user.0, v.chat.0, v.counter.name, v.value, v.target.0,],
                )
            })
            .await?;
        Ok(())
    }
}

impl CounterValueRepository for SqliteCounterValueRepository {}
