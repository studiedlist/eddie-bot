pub mod repository;

use crate::config::counters::Counter;
use teloxide::types::{UserId, ChatId};
use typesafe_repository::{GetIdentity, Identity, IdentityOf};

#[derive(Debug, Clone)]
pub struct CounterValue {
    value: i64,
    target: UserId,
    chat: ChatId,
    user: UserId,
    counter: Counter,
}

impl CounterValue {
    pub fn default_value() -> i64 {
        0
    }
    pub fn from_id(id: IdentityOf<CounterValue>, value: i64) -> Self {
        let (counter, user, target, chat) = id;
        Self {
            value,
            target,
            chat,
            user,
            counter,
        }
    }
}

impl Identity for CounterValue {
    type Id = (Counter, UserId, UserId, ChatId);
}

impl GetIdentity for CounterValue {
    fn id(&self) -> Self::Id {
        (self.counter.clone(), self.user, self.target, self.chat)
    }
}
