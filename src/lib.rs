#![deny(
    future_incompatible,
    let_underscore,
    keyword_idents,
    elided_lifetimes_in_paths,
    meta_variable_misuse,
    noop_method_call,
    pointer_structural_match,
    unused_lifetimes,
    unused_qualifications,
    unsafe_op_in_unsafe_fn,
    clippy::undocumented_unsafe_blocks,
    clippy::debug_assert_with_mut_call,
    clippy::empty_line_after_outer_attr,
    clippy::panic,
    clippy::unwrap_used,
    clippy::expect_used,
    clippy::redundant_field_names,
    clippy::rest_pat_in_fully_bound_structs,
    clippy::unneeded_field_pattern,
    clippy::useless_let_if_seq,
    clippy::default_union_representation,
    clippy::arithmetic_side_effects,
    clippy::checked_conversions,
    clippy::dbg_macro
)]
#![warn(
    clippy::pedantic,
    clippy::cloned_instead_of_copied,
    clippy::cognitive_complexity
)]
#![allow(
    clippy::must_use_candidate,
    clippy::doc_markdown,
    clippy::missing_errors_doc,
    clippy::implicit_hasher
)]

use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;

pub mod counters;
pub mod member;
pub mod motd;
pub mod rudelist;

pub mod checks;
pub mod config;
pub mod util;

pub mod filters;
pub mod handlers;

pub mod groups;

#[derive(Serialize, Deserialize, Debug, Clone, Hash, PartialEq, Eq)]
pub struct InputMessage {
    pub text: String,
    pub groups: Vec<String>,
}

pub fn parse_messages(path: &Path) -> Result<HashSet<InputMessage>, anyhow::Error> {
    let reader = BufReader::new(File::open(path)?);
    Ok(serde_yaml::from_reader(reader)?)
}

pub fn parse_blacklist(path: &Path) -> Result<Vec<u64>, anyhow::Error> {
    BufReader::new(File::open(path)?)
        .lines()
        .map(|line| Ok(line?.parse()?))
        .collect()
}
