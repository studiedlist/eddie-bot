use crate::config::Config;
use crate::member::repository::MemberRepository;
use crate::member::Member;
use crate::motd::repository::{Latest, Motd, MotdRepository};
use crate::util::typing_delay;
use chrono::Utc;
use rand::Rng;
use std::collections::HashMap;
use std::error::Error;
use std::sync::Arc;
use teloxide::prelude::Requester;
use teloxide::prelude::{Bot, Message};

pub async fn man_of_the_day(
    bot: Bot,
    msg: Message,
    config: Arc<Config>,
    motd_repository: Arc<dyn MotdRepository>,
    member_repository: Arc<dyn MemberRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let time_now = Utc::now().naive_utc();
    let chat_id = msg.chat.id;
    let motd = motd_repository.get_by(&Latest(chat_id)).await?;
    if let Some(m) = motd {
        if m.date.naive_utc().date() >= time_now.date() {
            let name = match m.username {
                Some(username) => format!("{}({})", m.name, username),
                None => m.name.clone(),
            };
            let text = config.messages.motd_exists.replace("$1", &name);
            bot.send_message(msg.chat.id, text).await?;
            return Ok(());
        }
    }
    let members: Vec<Member> = member_repository.list_by(&chat_id).await?;
    if members.is_empty() {
        bot.send_message(msg.chat.id, &config.messages.motd_not_enough_members)
            .await?;
        return Ok(());
    }
    let motd_index: usize = rand::thread_rng().gen_range(0..members.len());
    let Some(motd) = members.get(motd_index) else {
        log::error!("Member not found by index");
        return Ok(());
    };
    motd_repository
        .add(Motd::from((motd.clone(), Utc::now())))
        .await?;
    let name = motd
        .creds
        .username
        .as_ref()
        .map_or(motd.creds.name.clone(), |n| format!("@{n}"));

    bot.send_message(msg.chat.id, &config.messages.motd_rolling_started)
        .await?;
    let text = config.messages.motd_rolling_success.replace("$1", &name);
    typing_delay(&msg.chat.id, &bot, &text, config.typing_speed).await?;
    bot.send_message(msg.chat.id, &text).await?;
    Ok(())
}

pub async fn man_of_the_day_stat(
    bot: Bot,
    msg: Message,
    config: Arc<Config>,
    repository: Arc<dyn MotdRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let motd = repository.list_by(&msg.chat.id).await?;
    if motd.is_empty() {
        bot.send_message(msg.chat.id, &config.messages.motd_not_enough_members)
            .await?;
        return Ok(());
    }
    let chat_id = msg.chat.id;
    let map: HashMap<u64, (_, usize)> = HashMap::new();
    let motd = motd.into_iter().fold(map, |mut res, motd| {
        let id = motd.id.0;
        let r = res
            .get(&id)
            .map(|(_, v)| {
                v.checked_add(1).ok_or(anyhow::anyhow!(
                    "Maximum motd value reached. id: {id}; chat: {chat_id}"
                ))
            })
            .transpose()
            .unwrap_or_else(|err| {
                log::error!("{err}");
                None
            })
            .unwrap_or(1);
        // let r = res.get(&id).map_or(1, |(_, v)| v + 1);
        res.insert(id, (motd, r));
        res
    });
    let mut motd: Vec<_> = motd.values().collect();
    motd.sort_by(|(a, _), (b, _)| b.name.cmp(&a.name));
    motd.sort_by(|(_, a), (_, b)| b.cmp(a));
    let text = motd
        .iter()
        .fold(config.messages.motd_stat.clone(), |mut res, (m, c)| {
            res.push('\n');
            res.push_str(&format!("- {m} ({c})"));
            res
        });
    bot.send_message(msg.chat.id, text).await?;
    Ok(())
}
