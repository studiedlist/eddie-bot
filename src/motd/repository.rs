#![allow(clippy::module_name_repetitions)]

use crate::member::Member;
use chrono::{DateTime, NaiveDateTime, Utc};
use teloxide::types::{ChatId, UserId};
use typesafe_repository::prelude::*;

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct Motd {
    pub id: UserId,
    pub chat_id: ChatId,
    pub username: Option<String>,
    pub name: String,
    pub date: DateTime<Utc>,
}

impl From<(Member, DateTime<Utc>)> for Motd {
    fn from((m, date): (Member, DateTime<Utc>)) -> Self {
        Motd {
            id: m.id,
            chat_id: m.chat_id,
            username: m.creds.username,
            name: m.creds.name,
            date,
        }
    }
}

impl std::fmt::Display for Motd {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match &self.username {
            Some(username) => write!(f, "{} ({})", self.name, username),
            None => write!(f, "{}", self.name),
        }
    }
}

#[derive(Clone, Debug)]
pub struct MotdIdentity {
    pub id: UserId,
    pub chat_id: ChatId,
    pub date: DateTime<Utc>,
}

impl Identity for Motd {
    type Id = MotdIdentity;
}

impl GetIdentity for Motd {
    fn id(&self) -> Self::Id {
        MotdIdentity {
            id: self.id,
            chat_id: self.chat_id,
            date: self.date,
        }
    }
}

impl IdentityBy<ChatId> for Motd {
    fn id_by(&self) -> ChatId {
        self.chat_id
    }
}

impl IdentityBy<Latest> for Motd {
    fn id_by(&self) -> Latest {
        Latest(self.chat_id)
    }
}

pub struct Latest(pub ChatId);

pub trait MotdRepository:
    Repository<Motd, Error = anyhow::Error> + Add<Motd> + ListBy<Motd, ChatId> + GetBy<Motd, Latest>
where
    Self: Send + Sync,
{
}

use async_trait::async_trait;
use rusqlite::params;
use tokio_rusqlite::Connection;

pub struct SqliteMotdRepository {
    conn: Connection,
}

impl SqliteMotdRepository {
    pub async fn new(conn: Connection) -> Result<Self, rusqlite::Error> {
        conn.call(move |conn| {
            conn.execute(
                "CREATE TABLE IF NOT EXISTS man_of_the_day (date INTEGER, user_id INTEGER, username STRING, chat_id INTEGER, name STRING)",
                [],
            )
        }).await?;
        Ok(Self { conn })
    }
}

#[async_trait]
impl Add<Motd> for SqliteMotdRepository {
    async fn add(&self, motd: Motd) -> Result<(), Self::Error> {
        self.conn
            .call(move |conn| {
                conn.execute("INSERT INTO man_of_the_day (date, username, user_id, chat_id, name) VALUES (?1, ?2, ?3, ?4, ?5)",
                params![
                    Utc::now().timestamp(),
                    motd.username,
                    motd.id.0,
                    motd.chat_id.0,
                    motd.name
                ],
                )
            }).await?;
        Ok(())
    }
}

#[async_trait]
impl ListBy<Motd, ChatId> for SqliteMotdRepository {
    async fn list_by(&self, id: &ChatId) -> Result<Vec<Motd>, Self::Error> {
        let id = *id;
        let motd = self.conn
            .call(move |conn| {
                let mut stmt = conn.prepare("SELECT user_id, chat_id, username, name, date FROM man_of_the_day WHERE chat_id = ?1")?;
                let motd : Vec<Motd> = stmt
                    .query_and_then([id.0], |row| {
                        let date = DateTime::from_utc(
                            NaiveDateTime::from_timestamp_opt(row.get(4)?, 0)
                                .ok_or(anyhow::anyhow!("Cannot parse datetime from DB value"))?,
                            Utc
                        );
                        Ok::<_, Self::Error>(
                            Motd {
                                id: UserId(row.get(0)?),
                                chat_id: ChatId(row.get(1)?),
                                username: row.get(2)?,
                                name: row.get(3)?,
                                date,
                            }
                        )
                    })?
                    .filter_map(|e| {
                        e.ok()
                    })
                    .collect();
                Ok::<Vec<_>, Self::Error>(motd)
            }).await?;
        Ok(motd)
    }
}

#[async_trait]
impl GetBy<Motd, Latest> for SqliteMotdRepository {
    async fn get_by(&self, latest: &Latest) -> Result<Option<Motd>, Self::Error> {
        let chat_id = latest.0 .0;
        let motd : Option<Motd> = self.conn
            .call(move |conn| {
                let mut stmt = conn.prepare(
                    "SELECT user_id, chat_id, username, name, date FROM man_of_the_day WHERE chat_id = ?1 ORDER BY date DESC",
                )?;
                let motd : Option<Motd> = stmt
                    .query_and_then([chat_id], |row| {
                        let date = DateTime::from_utc(
                            NaiveDateTime::from_timestamp_opt(row.get(4)?, 0)
                                .ok_or(anyhow::anyhow!("Cannot parse datetime from DB value"))?,
                            Utc
                        );
                        Ok::<_, Self::Error>(
                            Motd {
                                id: UserId(row.get(0)?),
                                chat_id: ChatId(row.get(1)?),
                                username: row.get(2)?,
                                name: row.get(3)?,
                                date,
                            }
                        )
                    })?
                    .next()
                    .transpose()?;
                Ok::<_, Self::Error>(motd)
            }).await?;
        Ok(motd)
    }
}

impl Repository<Motd> for SqliteMotdRepository {
    type Error = anyhow::Error;
}

impl MotdRepository for SqliteMotdRepository {}
