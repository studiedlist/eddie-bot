use crate::InputMessage;
use regex::Regex;
use std::collections::HashSet;
use std::time::Duration;
use teloxide::prelude::*;
use teloxide::types::{ChatAction, ChatId, True};
use teloxide::RequestError;
use std::sync::Arc;
use tokio::sync::Notify;

pub fn get_by_message_group(
    messages: &HashSet<InputMessage>,
    group: Option<String>,
) -> Vec<&String> {
    let iter = messages.iter();
    match group {
        Some(group) => iter
            .filter(|&msg| msg.groups.len() == 1 && msg.groups.contains(&group))
            .map(|msg| &msg.text)
            .collect(),
        None => iter.map(|msg| &msg.text).collect(),
    }
}

pub fn get_by_message_groups<'a, T>(
    messages: &'a HashSet<InputMessage>,
    groups: &T,
) -> Vec<&'a String>
where
    for<'b> &'b T: IntoIterator<Item = &'b String>,
{
    let groups: HashSet<_> = groups.into_iter().collect();
    messages
        .iter()
        .filter(|&msg| {
            let msg_groups = msg.groups.iter().collect::<HashSet<_>>();
            groups.is_subset(&msg_groups)
        })
        .map(|msg| &msg.text)
        .collect()
}

pub fn filter_messages<'a>(messages: &[&'a String], keyword: &str) -> Vec<&'a String> {
    messages
        .iter()
        .filter_map(|elem| {
            if elem.contains(keyword) {
                return Some(*elem);
            }
            None
        })
        .collect()
}

pub fn filter_messages_regex<'a>(messages: &[&'a String], regex: &Regex) -> Vec<&'a String> {
    messages
        .iter()
        .filter_map(|elem| {
            if regex.is_match(elem) {
                return Some(*elem);
            }
            None
        })
        .collect()
}

pub fn calc_num(seed: usize, max: usize) -> usize {
    (seed.wrapping_mul(32)).wrapping_rem(max)
}

pub async fn get_admins(chat_id: &ChatId, bot: &Bot) -> Result<Vec<u64>, RequestError> {
    Ok(bot
        .get_chat_administrators(*chat_id)
        .send()
        .await?
        .iter()
        .map(|member| member.user.id.0)
        .collect())
}

pub async fn send_typing_status(chat_id: &ChatId, bot: &Bot) -> Result<True, RequestError> {
    bot.send_chat_action(*chat_id, ChatAction::Typing)
        .send()
        .await
}

pub async fn typing_delay(
    chat_id: &ChatId,
    bot: &Bot,
    text: &str,
    speed: u16,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    const REFRESH_DELAY: u64 = 4;
    #[allow(clippy::arithmetic_side_effects)]
    let delay = text.len() / (speed as usize / 60);
    let delay: u64 = delay.try_into()?;
    let max = match delay.wrapping_div(REFRESH_DELAY) {
        x if x > 1 => x,
        _ => 1,
    };
    for x in 0..max {
        send_typing_status(chat_id, bot).await?;
        let duration = if x == max.wrapping_sub(1) {
            Duration::from_secs(delay.wrapping_rem(REFRESH_DELAY))
        } else {
            Duration::from_secs(REFRESH_DELAY)
        };
        tokio::time::sleep(duration).await;
    }
    Ok(())
}

pub async fn type_until(
    chat_id: &ChatId,
    bot: &Bot,
    notify: Arc<Notify>,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    const REFRESH_DELAY: u64 = 4;
    let duration = Duration::from_secs(REFRESH_DELAY);
    #[allow(clippy::arithmetic_side_effects)]
    loop {
        tokio::select!(
            _ = send_typing_status(chat_id, bot) => (),
            _ = notify.notified() => return Ok(()),
        );
        tokio::select!(
            _ = tokio::time::sleep(duration) => (),
            _ = notify.notified() => return Ok(()),
        );
    }
}
