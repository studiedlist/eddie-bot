use crate::rudelist::repository::RudelistedRepository;
use crate::rudelist::Rudelisted;
use chrono::{Duration, Utc};
use std::error::Error;
use std::sync::Arc;
use teloxide::prelude::*;

static DEFAULT_RUDELISTED_TIME: i64 = 5;

pub async fn rudelist(
    msg: Message,
    repo: Arc<dyn RudelistedRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    if let (Some(msg), Some(text)) = (msg.reply_to_message(), msg.text()) {
        let Some(user) = msg.from() else {
            return Ok(())
        };
        let until = Utc::now();
        let regex = lazy_regex::regex!(r"\d+");
        let minutes = match regex.find(text) {
            Some(find) => find.as_str().parse()?,
            None => DEFAULT_RUDELISTED_TIME,
        };
        let duration = Duration::minutes(minutes);
        let Some(until) = until.checked_add_signed(duration) else {
            log::error!("Cannot calculate rudelist time");
            return Ok(())
        };

        repo.add(Rudelisted::new(user.id, msg.chat.id, until))
            .await?;
    }
    Ok(())
}

pub async fn unrudelist(
    msg: Message,
    repo: Arc<dyn RudelistedRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    if let Some(msg) = msg.reply_to_message() {
        if let Some(user) = msg.from() {
            repo.remove(&(msg.chat.id, user.id)).await?;
        }
    }
    Ok(())
}
