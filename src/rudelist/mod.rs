pub mod handlers;
pub mod repository;

use chrono::{DateTime, Utc};
use teloxide::types::{ChatId, UserId};
use typesafe_repository::prelude::*;

#[derive(Hash, PartialEq, Eq)]
pub struct Rudelisted {
    pub id: UserId,
    pub chat_id: ChatId,
    pub until: DateTime<Utc>,
}

impl Rudelisted {
    pub fn new(id: UserId, chat_id: ChatId, until: DateTime<Utc>) -> Self {
        Self { id, chat_id, until }
    }
}

impl Identity for Rudelisted {
    type Id = (ChatId, UserId);
}

impl GetIdentity for Rudelisted {
    fn id(&self) -> IdentityOf<Rudelisted> {
        (self.chat_id, self.id)
    }
}

impl IdentityBy<ChatId> for Rudelisted {
    fn id_by(&self) -> ChatId {
        self.chat_id
    }
}
