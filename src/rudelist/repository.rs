#![allow(clippy::module_name_repetitions)]

use super::Rudelisted;
use async_trait::async_trait;
use chrono::{DateTime, NaiveDateTime, Utc};
use std::error::Error;
use teloxide::types::{ChatId, UserId};
use typesafe_repository::prelude::*;

#[async_trait]
pub trait RudelistedRepository:
    Repository<Rudelisted, Error = Box<dyn Error + Send + Sync>>
    + Add<Rudelisted>
    + Remove<Rudelisted>
    + ListBy<Rudelisted, ChatId>
    + List<Rudelisted>
where
    Self: Send + Sync,
{
    async fn clean_rudelisted(&self) -> Result<(), <Self as Repository<Rudelisted>>::Error>;
}

use rusqlite::params;
use tokio_rusqlite::Connection;

pub struct SqliteRudelistedRepository {
    pub conn: Connection,
}

impl SqliteRudelistedRepository {
    pub async fn new(conn: Connection) -> Result<Self, rusqlite::Error> {
        conn.call(move |conn| {
            conn.execute(
                "CREATE TABLE IF NOT EXISTS rudelist (id INTEGER, chat_id INTEGER, until INTEGER)",
                [],
            )
        })
        .await?;
        Ok(Self { conn })
    }
}

impl Repository<Rudelisted> for SqliteRudelistedRepository {
    type Error = Box<dyn Error + Send + Sync>;
}

#[async_trait]
impl Add<Rudelisted> for SqliteRudelistedRepository {
    async fn add(&self, r: Rudelisted) -> Result<(), Self::Error> {
        self.conn
            .call(move |conn| {
                conn.execute(
                    "INSERT INTO rudelist (id, chat_id, until) VALUES (?1, ?2, ?3)",
                    params![r.id.0, r.chat_id.0, r.until.timestamp()],
                )
            })
            .await?;
        Ok(())
    }
}

#[async_trait]
impl Remove<Rudelisted> for SqliteRudelistedRepository {
    async fn remove(&self, id: &IdentityOf<Rudelisted>) -> Result<Option<Rudelisted>, Self::Error> {
        let (id, chat_id) = *id;
        self.conn
            .call(move |conn| {
                conn.execute(
                    "DELETE FROM rudelist WHERE id=?1 AND chat_id=?2",
                    params![id.0, chat_id.0],
                )
            })
            .await?;
        Ok(None)
    }
}

#[async_trait]
impl ListBy<Rudelisted, ChatId> for SqliteRudelistedRepository {
    async fn list_by(&self, id: &ChatId) -> Result<Vec<Rudelisted>, Self::Error> {
        let id = *id;
        let res = self
            .conn
            .call(move |conn| {
                let mut stmt =
                    conn.prepare("SELECT id, chat_id, until FROM rudelist WHERE chat_id = ?1")?;
                let iter = stmt.query_and_then([id.0], |row| {
                    let timestamp = row.get(2)?;
                    Ok::<_, Self::Error>(Rudelisted {
                        id: UserId(row.get(0)?),
                        chat_id: ChatId(row.get(1)?),
                        until: DateTime::from_utc(
                            NaiveDateTime::from_timestamp_opt(timestamp, 0)
                                .ok_or(anyhow::anyhow!("Cannot parse datetime from DB value"))?,
                            Utc,
                        ),
                    })
                })?;
                Ok::<_, Self::Error>(
                    iter.filter_map(|elem| match elem {
                        Ok(elem) => Some(elem),
                        Err(_) => None,
                    })
                    .collect(),
                )
            })
            .await?;
        Ok(res)
    }
}

#[async_trait]
impl List<Rudelisted> for SqliteRudelistedRepository {
    async fn list(&self) -> Result<Vec<Rudelisted>, Self::Error> {
        let res = self
            .conn
            .call(move |conn| {
                let mut stmt = conn.prepare("SELECT id, chat_id, until FROM rudelist")?;
                let iter = stmt.query_and_then([], |row| {
                    let timestamp = row.get(2)?;
                    Ok::<_, Self::Error>(Rudelisted {
                        id: UserId(row.get(0)?),
                        chat_id: ChatId(row.get(1)?),
                        until: DateTime::from_utc(
                            NaiveDateTime::from_timestamp_opt(timestamp, 0)
                                .ok_or(anyhow::anyhow!("Cannot parse datetime from DB value"))?,
                            Utc,
                        ),
                    })
                })?;
                Ok::<_, Self::Error>(
                    iter.filter_map(|elem| match elem {
                        Ok(elem) => Some(elem),
                        Err(_) => None,
                    })
                    .collect(),
                )
            })
            .await?;
        Ok(res)
    }
}

#[async_trait]
impl RudelistedRepository for SqliteRudelistedRepository {
    async fn clean_rudelisted(&self) -> Result<(), <Self as Repository<Rudelisted>>::Error> {
        let rudelisted = self.list().await?;
        let time_now = Utc::now();
        let invalid = rudelisted.iter().filter(|r| r.until < time_now);
        for r in invalid {
            self.remove(&r.id()).await?;
        }
        Ok(())
    }
}
