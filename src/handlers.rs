use crate::config::counters::{Counter, CounterEvent};
use crate::config::{Config, Expand, Reply, ReplyRegex, ReplyText, ReplyVariant};
use crate::counters::repository::CounterValueRepository;
use crate::groups::repository::GroupRepository;
use crate::member::repository::MemberRepository;
use crate::member::{Admin, Member};
use crate::util::{
    calc_num, filter_messages_regex, get_by_message_groups, type_until, typing_delay,
};
use crate::InputMessage;
use bot_utils::{find_similar, Similarity};
use rand::Rng;
use sedregex::ReplaceCommand;
use std::collections::HashSet;
use std::error::Error;
use std::path::PathBuf;
use std::sync::Arc;
use std::time::Duration;
use teloxide::prelude::*;
use teloxide::types::{InputFile, ParseMode};
use teloxide::utils::markdown::escape;
use tokio::sync::Notify;

static MAX_MESSAGE_LEN: usize = 4096;

pub async fn answer(
    messages: HashSet<InputMessage>,
    bot: Bot,
    msg: Message,
    config: Arc<Config>,
    repo: Arc<dyn GroupRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let notify = Arc::new(Notify::new());
    let b = bot.clone();
    let n = notify.clone();
    tokio::spawn(async move {
        match tokio::time::timeout(Duration::from_secs(15), type_until(&msg.chat.id, &b, n)).await {
            Ok(Err(err)) => log::error!("Typing error:\n{}", err),
            Err(err) => log::error!("Timeout error:\n{}", err),
            _ => (),
        }
    });
    let groups: Vec<String> = repo
        .list_by(&msg.chat.id)
        .await?
        .into_iter()
        .map(|g| g.name)
        .collect();
    let messages = get_by_message_groups(&messages, &groups);
    #[allow(clippy::cast_sign_loss)]
    let calc_num = |x| calc_num(msg.id.0 as usize, x);
    let message = match msg.text() {
        Some(text) => {
            let text = text
                .to_lowercase()
                .replace(&config.name, "")
                .replace(',', "");
            match find_similar(&messages, &text, Similarity::Low) {
                Some(similar) if similar.len() > 10 => messages[calc_num(messages.len())],
                _ => messages[calc_num(messages.len())],
            }
        }
        None => messages[calc_num(messages.len())],
    };
    notify.notify_waiters();
    typing_delay(&msg.chat.id, &bot, message, config.typing_speed).await?;
    bot.send_message(msg.chat.id, escape(message))
        .parse_mode(ParseMode::MarkdownV2)
        .reply_to_message_id(msg.id)
        .await?;
    Ok(())
}

pub async fn message_reply(
    messages: HashSet<InputMessage>,
    bot: Bot,
    msg: Message,
    config: Arc<Config>,
    repo: Arc<dyn GroupRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let notify = Arc::new(Notify::new());
    let b = bot.clone();
    let n = notify.clone();
    tokio::spawn(async move {
        match tokio::time::timeout(Duration::from_secs(15), type_until(&msg.chat.id, &b, n)).await {
            Ok(Err(err)) => log::error!("Typing error:\n{}", err),
            Err(err) => log::error!("Timeout error:\n{}", err),
            _ => (),
        }
    });
    let groups: Vec<String> = repo
        .list_by(&msg.chat.id)
        .await?
        .into_iter()
        .map(|g| g.name)
        .collect();
    let messages = get_by_message_groups(&messages, &groups);
    #[allow(clippy::cast_sign_loss)]
    let calc_num = |x| calc_num(msg.id.0 as usize, x);
    let message = match msg.text() {
        Some(text) => {
            let text = text
                .to_lowercase()
                .replace(&config.name, "")
                .replace(',', "");
            match find_similar(&messages, &text, Similarity::Low) {
                Some(similar) if similar.len() > 10 => similar[calc_num(similar.len())],
                _ => messages[calc_num(messages.len())],
            }
        }
        None => messages[calc_num(messages.len())],
    };
    notify.notify_waiters();
    typing_delay(&msg.chat.id, &bot, message, config.typing_speed).await?;
    bot.send_message(msg.chat.id, escape(message))
        .parse_mode(ParseMode::MarkdownV2)
        .reply_to_message_id(msg.id)
        .await?;
    Ok(())
}

pub async fn question(
    messages: HashSet<InputMessage>,
    bot: Bot,
    msg: Message,
    config: Arc<Config>,
    repo: Arc<dyn GroupRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let notify = Arc::new(Notify::new());
    let b = bot.clone();
    let n = notify.clone();
    tokio::spawn(async move {
        match tokio::time::timeout(Duration::from_secs(15), type_until(&msg.chat.id, &b, n)).await {
            Ok(Err(err)) => log::error!("Typing error:\n{}", err),
            Err(err) => log::error!("Timeout error:\n{}", err),
            _ => (),
        }
    });
    let groups: Vec<String> = repo
        .list_by(&msg.chat.id)
        .await?
        .into_iter()
        .map(|g| g.name)
        .collect();
    let messages = get_by_message_groups(&messages, &groups);
    let filtered = filter_messages_regex(&messages, &config.regex.question);
    #[allow(clippy::cast_sign_loss)]
    let calc_num = |x| calc_num(msg.id.0 as usize, x);
    let message = match filtered.len() {
        x if x < 10 => messages[calc_num(messages.len())],
        _ => match msg.text() {
            Some(text) => match find_similar(&filtered, text, Similarity::Low) {
                Some(similar) if similar.len() > 10 => similar[calc_num(similar.len())],
                _ => filtered[calc_num(filtered.len())],
            },
            None => filtered[calc_num(filtered.len())],
        },
    };
    notify.notify_waiters();
    typing_delay(&msg.chat.id, &bot, message, config.typing_speed).await?;
    bot.send_message(msg.chat.id, escape(message)).await?;
    Ok(())
}

pub async fn kick(
    messages: HashSet<InputMessage>,
    bot: Bot,
    msg: Message,
    config: Arc<Config>,
    repository: Arc<dyn MemberRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let Some(user) = msg.from() else {
        log::error!("Message with no sender");
        return Ok(())
    };
    let admins = repository.list_by(&msg.chat.id).await;
    let admins: Vec<Admin> = match admins {
        Ok(admins) => admins,
        Err(err) => {
            log::error!("Cannot get admins of chat\n{}", err);
            return Ok(());
        }
    };
    if admins.iter().any(|x| x.user_id == user.id) {
        rude_answer(messages, bot, msg, config).await?;
    } else {
        bot.kick_chat_member(msg.chat.id, user.id).await?;
        bot.unban_chat_member(msg.chat.id, user.id).await?;
    }
    Ok(())
}

pub async fn rude_answer(
    messages: HashSet<InputMessage>,
    bot: Bot,
    msg: Message,
    config: Arc<Config>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let notify = Arc::new(Notify::new());
    let b = bot.clone();
    let n = notify.clone();
    tokio::spawn(async move {
        match tokio::time::timeout(Duration::from_secs(15), type_until(&msg.chat.id, &b, n)).await {
            Ok(Err(err)) => log::error!("Typing error:\n{}", err),
            Err(err) => log::error!("Timeout error:\n{}", err),
            _ => (),
        }
    });
    let messages: Vec<&String> = messages.iter().map(|msg| &msg.text).collect();
    let filtered = filter_messages_regex(&messages, &config.regex.rude_answer);
    if filtered.is_empty() {
        log::warn!("No rude messages were found");
    } else {
        notify.notify_waiters();
        #[allow(clippy::cast_sign_loss)]
        let message = filtered[calc_num(msg.id.0 as usize, filtered.len())];
        let typing_speed = if let Some(speed) = config.typing_speed.checked_mul(3) {
            speed
        } else {
            log::error!("Overflow calculating typing speed");
            config.typing_speed
        };
        typing_delay(&msg.chat.id, &bot, message, typing_speed).await?;
        bot.send_message(msg.chat.id, escape(message))
            .parse_mode(ParseMode::MarkdownV2)
            .reply_to_message_id(msg.id)
            .await?;
    }
    Ok(())
}

const SUBSTITUTION_SEPARATOR: char = '/';

pub async fn substitution(
    messages: HashSet<InputMessage>,
    bot: Bot,
    config: Arc<Config>,
    msg: Message,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    const DELETE_FLAG: char = 'd';
    if let Some(r_msg) = msg.reply_to_message() {
        let Some(text) = r_msg.text().or_else(|| r_msg.caption()) else {
            log::warn!("Message with no text {:?}", r_msg);
            return Ok(());
        };
        let query = match msg.text() {
            Some(query) => String::from(query),
            None => return Ok(()),
        };
        if query.chars().any(|x| x.len_utf8() > 2) {
            return rude_answer(messages, bot, msg, config).await;
        }
        let mut result = text.to_string();
        for query in query.split('\n') {
            if !config.regex.substitution.is_match(query) {
                continue;
            }
            let mut query = query.to_string();
            if query.rmatches(SUBSTITUTION_SEPARATOR).count() > 2 {
                let mut split: Vec<_> = query.split(SUBSTITUTION_SEPARATOR).collect();
                let mut args =
                    String::from(split.pop().ok_or(anyhow::anyhow!("No elements in split"))?);
                if args.contains(DELETE_FLAG) {
                    bot.delete_message(msg.chat.id, msg.id).await?;
                    args = args.replace(DELETE_FLAG, "");
                }
                let mut q = String::new();
                for e in split {
                    q.push_str(e);
                    q.push(SUBSTITUTION_SEPARATOR);
                }
                q.push_str(&args);
                query = q;
            }
            result = match ReplaceCommand::new(&query) {
                Ok(cmd) => String::from(cmd.execute(result)),
                Err(err) => {
                    bot.send_message(msg.chat.id, &format!("{err:?}"))
                        .reply_to_message_id(msg.id)
                        .await?;
                    return Ok(());
                }
            };
        }
        for t in split_message(&result, MAX_MESSAGE_LEN) {
            let result = bot
                .send_message(msg.chat.id, t)
                .parse_mode(ParseMode::MarkdownV2)
                .reply_to_message_id(r_msg.id)
                .await;
            if result.is_err() {
                bot.send_message(msg.chat.id, t)
                    .reply_to_message_id(r_msg.id)
                    .await?;
            }
        }
    }
    Ok(())
}

fn split_message(msg: &str, len: usize) -> Vec<&str> {
    if msg.len() < len {
        return vec![msg];
    }
    let mut buf = msg;
    let mut result = vec![];
    loop {
        let (r, t) = buf.split_at(len);
        result.push(r);
        buf = t;
        if buf.len() < len {
            result.push(buf);
            break;
        }
    }
    result
}

pub async fn expand(
    bot: Bot,
    msg: Message,
    config: Arc<Config>,
    bot_id: u64,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let Some(text) = msg.text().or_else(|| msg.caption()) else {
        return Ok(())
    };
    let matches_class = |expand: &Expand| {
        expand
            .msg_class
            .as_ref()
            .map_or(true, |c| c.matches(&msg, bot_id))
    };
    for expand in &config.expands {
        if matches_class(expand)
            && expand.regex.is_match(text)
            && ((expand.chance - 1.0).abs() < 0.1
                || rand::thread_rng().gen::<f32>() < expand.chance)
        {
            let Some(text) = expand.replace(text, &msg, &config) else {
                log::error!("Cannot replace expand {:?}\n{:?}", &expand, msg);
                return Ok(());
            };
            let send_message = bot
                .send_message(msg.chat.id, escape(&text))
                .parse_mode(ParseMode::MarkdownV2);
            if expand.delete {
                bot.delete_message(msg.chat.id, msg.id).await?;
                send_message.await?;
            } else {
                send_message.reply_to_message_id(msg.id).await?;
            }
            return Ok(());
        }
    }
    Ok(())
}

pub async fn reply(
    bot: Bot,
    bot_id: u64,
    msg: Message,
    config: Arc<Config>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let Some(text) = msg.text().or_else(|| msg.caption()) else { return Ok(()) };
    let send = |t: &str| {
        bot.send_message(msg.chat.id, t)
            .parse_mode(ParseMode::MarkdownV2)
            .reply_to_message_id(msg.id)
    };
    let send_video = |p: &PathBuf| {
        let file = InputFile::file(p);
        bot.send_video(msg.chat.id, file)
            .reply_to_message_id(msg.id)
    };
    let send_gif = |p: &PathBuf| {
        let file = InputFile::file(p);
        bot.send_animation(msg.chat.id, file)
            .reply_to_message_id(msg.id)
    };
    let send_picture = |p: &PathBuf| {
        let file = InputFile::file(p);
        bot.send_photo(msg.chat.id, file)
            .reply_to_message_id(msg.id)
    };
    let send_voice = |p: &PathBuf| {
        let file = InputFile::file(p);
        bot.send_voice(msg.chat.id, file)
            .reply_to_message_id(msg.id)
    };
    let matches_class = |r: &Reply| {
        r.msg_class
            .as_ref()
            .map_or(true, |c| c.matches(&msg, bot_id))
    };
    for reply in &config.replies {
        let chance_matched = reply
            .chance
            .map_or(true, |c| rand::thread_rng().gen::<f32>() < c);
        let reply_matched = match &reply.regex {
            ReplyRegex::Text(regex) => regex.is_match(text),
            ReplyRegex::List(list) => {
                let r = list.iter().find(|regex| regex.is_match(text));
                r.is_some()
            }
        };
        if matches_class(reply) && chance_matched && reply_matched {
            match &reply.reply {
                ReplyVariant::Text(t) => {
                    send(t).await?;
                }
                ReplyVariant::Video(p) => {
                    send_video(p).await?;
                }
                ReplyVariant::Gif(p) => {
                    send_gif(p).await?;
                }
                ReplyVariant::Picture(p) => {
                    send_picture(p).await?;
                }
                ReplyVariant::Voice(p) => {
                    send_voice(p).await?;
                }
                ReplyVariant::Texts(v) => {
                    let elem = v
                        .iter()
                        .find(|t| t.1.map_or(true, |c| rand::thread_rng().gen::<f32>() < c));
                    if let Some(t) = elem {
                        match &t.0 {
                            ReplyText::Text(t) => send(t).await?,
                            ReplyText::Video(p) => send_video(p).await?,
                            ReplyText::Gif(p) => send_gif(p).await?,
                            ReplyText::Picture(p) => send_picture(p).await?,
                            ReplyText::Voice(p) => send_voice(p).await?,
                        };
                    }
                }
            }
            return Ok(());
        }
    }
    Ok(())
}

pub async fn list_members(
    bot: Bot,
    msg: Message,
    config: Arc<Config>,
    repo: Arc<dyn MemberRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let members: Vec<Member> = repo.list_by(&msg.chat.id).await?;
    let text = match members {
        m if !m.is_empty() => m.iter().fold(
            config.messages.members_list_message.clone(),
            |mut res, m| {
                let m = &m.creds;
                res.push('\n');
                res.push_str(&format!("- {m}"));
                res
            },
        ),
        _ => config.messages.members_list_empty.clone(),
    };
    bot.send_message(msg.chat.id, text).await?;
    Ok(())
}

pub async fn counter(
    bot: Bot,
    msg: Message,
    c: Arc<(CounterEvent, Counter)>,
    repo: Arc<dyn CounterValueRepository>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    let (event, counter) = &*c;
    let Some(member) = Member::from_message(&msg) else { return Ok(()) };
    let target = match msg.reply_to_message() {
        Some(msg) => match msg.from() {
            Some(t) => t.id,
            None => return Ok(()),
        },
        None => return Ok(()),
    };
    let identity = (counter.clone(), member.id, target, msg.chat.id);
    let count = match event {
        CounterEvent::Increase => repo.increase(&identity).await,
        CounterEvent::Decrease => repo.decrease(&identity).await,
    }?;
    let Some(text) = counter.format_with_message(&msg, event, count) else { return Ok(()) };
    bot.send_message(msg.chat.id, text).await?;
    Ok(())
}

#[cfg(test)]
mod test {

    use super::*;

    #[test]
    fn splits_message() {
        let msg = (0..MAX_MESSAGE_LEN + MAX_MESSAGE_LEN / 2).fold(String::new(), |mut r, _| {
            r.push_str("t");
            r
        });
        let splitted = split_message(&msg, MAX_MESSAGE_LEN);
        assert_eq!(splitted.len(), 2);
        assert_eq!(splitted[0].len(), MAX_MESSAGE_LEN);
        assert_eq!(splitted[1].len(), MAX_MESSAGE_LEN / 2);

        let msg = (0..MAX_MESSAGE_LEN / 2).fold(String::new(), |mut r, _| {
            r.push_str("t");
            r
        });
        let splitted = split_message(&msg, MAX_MESSAGE_LEN);
        assert_eq!(splitted.len(), 1);
        assert_eq!(splitted[0].len(), MAX_MESSAGE_LEN / 2);
    }
}
