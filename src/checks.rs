use crate::rudelist::Rudelisted;
use crate::util::get_admins;
use chrono::Utc;
use teloxide::prelude::*;
use teloxide::types::{Message, User};

static DEFAULT_OUTDATED_TIME: i64 = 5;

pub fn is_blacklisted(msg: &Message, blacklist: &[u64]) -> bool {
    if let Some(from) = &msg.from() {
        if blacklist.contains(&from.id.0) {
            return true;
        }
    }
    false
}

pub fn is_reply_to_bot(msg: &Message, bot_id: &u64) -> bool {
    if let Some(reply) = msg.reply_to_message() {
        if let Some(user) = reply.from() {
            if user.id.0 == *bot_id {
                return true;
            }
        }
    }
    false
}

pub fn is_outdated(msg: &Message) -> bool {
    Utc::now().signed_duration_since(msg.date).num_minutes() > DEFAULT_OUTDATED_TIME
}

pub async fn is_admin(msg: Message, bot: Bot) -> bool {
    let admins = match get_admins(&msg.chat.id, &bot).await {
        Ok(admins) => admins,
        Err(err) => {
            log::error!("Cannot get admins of chat\n{}", err);
            return false;
        }
    };
    match msg.from() {
        Some(user) => admins.contains(&user.id.0),
        None => false,
    }
}

pub fn is_rudelisted<T>(user: &User, chat_id: ChatId, list: &T) -> bool
where
    for<'a> &'a T: IntoIterator<Item = &'a Rudelisted>,
{
    list.into_iter()
        .filter(|&r| r.id == user.id && r.chat_id == chat_id)
        .count()
        > 0
}
