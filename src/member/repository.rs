#![allow(clippy::module_name_repetitions)]

use typesafe_repository::prelude::*;

use super::{Admin, Credentials, Member, MemberIdentity};
use std::error::Error;
use teloxide::prelude::{Request, Requester};
use teloxide::types::{ChatId, UserId};

#[async_trait]
pub trait MemberRepository:
    Repository<Member, Error = Box<dyn Error + Send + Sync>>
    + Repository<Admin, Error = Box<dyn Error + Send + Sync>>
    + ListBy<Admin, ChatId>
    + ListBy<Member, ChatId>
    + Add<Member>
    + Remove<Member>
    + Update<Member>
where
    Self: Send + Sync,
{
    async fn clean_members(
        &self,
        chat_id: &ChatId,
    ) -> Result<Vec<Member>, <Self as Repository<Member>>::Error>;
}

use async_trait::async_trait;
use chrono::{DateTime, NaiveDateTime, Utc};
use rusqlite::params;
use teloxide::Bot;
use tokio_rusqlite::Connection;

pub struct SqliteMemberRepository {
    conn: Connection,
    bot: Bot,
}

impl SqliteMemberRepository {
    pub async fn new(conn: Connection, bot: Bot) -> Result<Self, rusqlite::Error> {
        conn.call(move |conn| {
            conn.execute(
                "CREATE TABLE IF NOT EXISTS members (last_active INTEGER, username STRING, user_id INTEGER, chat_id INTEGER, name STRING)",
                [],
            )
        }).await?;
        Ok(Self { conn, bot })
    }
}

impl Repository<Member> for SqliteMemberRepository {
    type Error = Box<dyn Error + Send + Sync>;
}

impl Repository<Admin> for SqliteMemberRepository {
    type Error = Box<dyn Error + Send + Sync>;
}

#[async_trait]
impl ListBy<Member, ChatId> for SqliteMemberRepository {
    async fn list_by(&self, chat_id: &ChatId) -> Result<Vec<Member>, Self::Error> {
        let chat_id = chat_id.0;
        let members = self.conn.call(move |conn| {
            let mut stmt = conn.prepare(
                "SELECT username, user_id, chat_id, name, last_active FROM members WHERE chat_id = ?1",
            )?;
            let iter = stmt.query_and_then([chat_id], |row| {
                let last_active = DateTime::from_utc(
                    NaiveDateTime::from_timestamp_opt(row.get(4)?, 0)
                        .ok_or(anyhow::anyhow!("Cannot parse datetime from DB value"))?,
                    Utc
                );
                Ok::<_, Self::Error>(Member {
                    id: UserId(row.get(1)?),
                    chat_id: ChatId(row.get(2)?),
                    creds: Credentials {
                        username: row.get(0).ok(),
                        name: row.get(3)?,
                    },
                    last_active,
                })
            })?;
            Ok::<_, Self::Error>(iter.filter_map(Result::ok).collect())
        })
        .await?;
        Ok(members)
    }
}

#[async_trait]
impl Update<Member> for SqliteMemberRepository {
    async fn update(&self, member: Member) -> Result<(), Self::Error> {
        self.conn.call(move |conn| {
            conn.execute(
                "UPDATE members SET last_active = ?1, username = ?2, user_id = ?3, chat_id = ?4, name = ?5 WHERE user_id = ?3 AND chat_id = ?4",
                params![
                    member.last_active,
                    member.creds.username,
                    member.id.0,
                    member.chat_id.0,
                    member.creds.name,
                ],
            )
            .map(|_| ())
        }).await?;
        Ok(())
    }
}

#[async_trait]
impl Add<Member> for SqliteMemberRepository {
    async fn add(&self, member: Member) -> Result<(), Self::Error> {
        self.conn.call(move |conn| {
            conn.execute(
                "INSERT INTO members (last_active, username, user_id, chat_id, name) VALUES (?1, ?2, ?3, ?4, ?5)",
                params![
                    Utc::now().timestamp(),
                    member.creds.username,
                    member.id.0,
                    member.chat_id.0,
                    member.creds.name,
                ],
            )
            .map(|_| ())
        }).await?;
        Ok(())
    }
}

#[async_trait]
impl Remove<Member> for SqliteMemberRepository {
    async fn remove(&self, id: &MemberIdentity) -> Result<Option<Member>, Self::Error> {
        let id = *id;
        self.conn
            .call(move |conn| {
                conn.execute(
                    "DELETE FROM members WHERE user_id=?1 AND chat_id=?2",
                    params![id.0 .0, id.1 .0],
                )
            })
            .await?;
        Ok(None)
    }
}

#[async_trait]
impl ListBy<Admin, ChatId> for SqliteMemberRepository {
    async fn list_by(&self, chat_id: &ChatId) -> Result<Vec<Admin>, Self::Error> {
        let chat_id = *chat_id;
        Ok(self
            .bot
            .get_chat_administrators(chat_id)
            .send()
            .await?
            .iter()
            .map(|member| Admin {
                user_id: member.user.id,
                chat_id,
            })
            .collect())
    }
}

#[async_trait]
impl MemberRepository for SqliteMemberRepository {
    async fn clean_members(
        &self,
        chat_id: &ChatId,
    ) -> Result<Vec<Member>, <Self as Repository<Member>>::Error> {
        use chrono::Duration;
        let members: Vec<Member> = self.list_by(chat_id).await?;
        let time_now = Utc::now();
        let invalid: Vec<_> = members
            .iter()
            .filter(|m| {
                m.last_active
                    .checked_add_signed(Duration::hours(48))
                    .map_or(true, |t| t < time_now)
            })
            .collect();
        for m in invalid {
            self.remove(&m.id()).await?;
        }
        Ok(members)
    }
}
