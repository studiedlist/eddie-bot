use chrono::{DateTime, Utc};
use teloxide::types::{ChatId, UserId};
use typesafe_repository::prelude::*;

pub mod repository;

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct Member {
    pub id: UserId,
    pub chat_id: ChatId,
    pub creds: Credentials,
    pub last_active: DateTime<Utc>,
}

impl Member {
    pub fn new(
        id: UserId,
        chat_id: ChatId,
        creds: Credentials,
        last_active: Option<DateTime<Utc>>,
    ) -> Member {
        Member {
            id,
            chat_id,
            creds,
            last_active: last_active.unwrap_or_else(Utc::now),
        }
    }
    pub fn from_message(msg: &teloxide::types::Message) -> Option<Member> {
        let user = msg.from()?;
        Some(Member::new(
            user.id,
            msg.chat.id,
            Credentials::new(
                user.username.clone(),
                user.first_name.clone(),
                user.last_name.clone(),
            ),
            Some(msg.date),
        ))
    }
}

type MemberIdentity = (UserId, ChatId);

impl Identity for Member {
    type Id = MemberIdentity;
}

impl GetIdentity for Member {
    fn id(&self) -> IdentityOf<Member> {
        (self.id, self.chat_id)
    }
}

impl Identity for Admin {
    type Id = MemberIdentity;
}

impl GetIdentity for Admin {
    fn id(&self) -> IdentityOf<Member> {
        (self.user_id, self.chat_id)
    }
}

impl IdentityBy<ChatId> for Member {
    fn id_by(&self) -> ChatId {
        self.chat_id
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct Admin {
    pub user_id: UserId,
    pub chat_id: ChatId,
}

impl IdentityBy<ChatId> for Admin {
    fn id_by(&self) -> ChatId {
        self.chat_id
    }
}

type Username = Option<String>;

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub struct Credentials {
    pub username: Username,
    pub name: String,
}

impl Credentials {
    pub fn new(username: Username, first_name: String, last_name: Option<String>) -> Self {
        Self {
            username,
            name: Self::format_name(first_name, last_name),
        }
    }
    pub fn format_name(first_name: String, last_name: Option<String>) -> String {
        last_name
            .map(|l| format!("{first_name} {l}"))
            .unwrap_or(first_name)
    }
}

impl std::fmt::Display for Credentials {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        match &self.username {
            Some(username) => write!(f, "{} ({})", self.name, username),
            None => write!(f, "{}", self.name),
        }
    }
}
