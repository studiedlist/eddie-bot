#![allow(clippy::needless_pass_by_value)]

use crate::checks::{is_blacklisted, is_outdated, is_reply_to_bot};
use crate::config::{
    counters::{Counter, CounterEvent},
    Command, Config, ReplyRegex,
};
use crate::member::repository::MemberRepository;
use crate::member::{Credentials, Member};
use crate::rudelist::repository::RudelistedRepository;
use once_cell::sync::Lazy;
use regex::Regex;
use std::sync::atomic::{AtomicU16, Ordering};
use std::sync::Arc;
use teloxide::prelude::*;

static MESSAGES_COUNT: Lazy<AtomicU16> = Lazy::new(AtomicU16::default);

pub fn not_blacklisted(msg: Message, blacklist: Vec<u64>) -> bool {
    !is_blacklisted(&msg, &blacklist)
}

pub fn not_outdated(msg: Message) -> bool {
    !is_outdated(&msg)
}

pub fn not_forwarded(msg: Message) -> bool {
    msg.forward().is_none()
}

pub fn reply_to_bot(msg: Message, bot_id: u64) -> bool {
    is_reply_to_bot(&msg, &bot_id)
}

pub fn reply_not_to_bot(msg: Message, bot_id: u64) -> bool {
    if msg.reply_to_message().is_none() {
        return false;
    }
    if is_reply_to_bot(&msg, &bot_id) {
        return false;
    }
    true
}

pub fn matches_substitution(msg: Message, config: Arc<Config>) -> bool {
    if msg.reply_to_message().is_none() {
        return false;
    }
    check_regex(&msg, &config.regex.substitution)
}

pub fn fuck_you(msg: Message, config: Arc<Config>) -> bool {
    let regex = &config.regex.fuck_you;
    match (msg.text(), msg.voice()) {
        (Some(text), _) => regex.is_match(text),
        (_, Some(_)) => (msg.id.0.wrapping_mul(32)) % 20 > 3,
        _ => false,
    }
}

pub fn answer(msg: Message, config: Arc<Config>) -> bool {
    let regex = &config.regex.answer;
    if msg.chat.is_private() {
        return true;
    }
    if let Some(text) = &msg.text() {
        return regex.is_match(text);
    }
    false
}

pub fn question_counter(msg: Message, config: Arc<Config>) -> bool {
    let question_delay = config.question_delay;
    if !msg.chat.is_group() && !msg.chat.is_supergroup() {
        return false;
    }
    if MESSAGES_COUNT.fetch_add(1, Ordering::Relaxed) >= question_delay {
        let max = if let Some(max) = question_delay.checked_div(2) {
            max
        } else {
            log::error!("Overflow calculating max message skip count");
            question_delay
        };
        match msg.id.0.try_into() {
            Ok(id) => {
                let id: u16 = id;
                MESSAGES_COUNT.store(id.wrapping_rem(max), Ordering::Relaxed);
            }
            Err(err) => {
                log::error!("Unable to convert message id to u16:\n{err}");
                MESSAGES_COUNT.store(0, Ordering::Relaxed);
            }
        };
        return true;
    }
    false
}

pub fn matches_expand(msg: Message, config: Arc<Config>) -> bool {
    let Some(text) = msg.text() else {
        return false
    };
    for expand in &config.expands {
        if expand.regex.is_match(text) {
            return true;
        }
    }
    false
}

pub fn matches_reply(msg: Message, config: Arc<Config>) -> bool {
    let Some(text) = msg.text() else {
        return false
    };
    for reply in &config.replies {
        match &reply.regex {
            ReplyRegex::Text(regex) => {
                if regex.is_match(text) {
                    return true;
                }
            }
            ReplyRegex::List(list) => {
                let r = list.iter().find(|regex| regex.is_match(text));
                if r.is_some() {
                    return true;
                }
            }
        }
    }
    false
}

pub async fn is_rudelisted(msg: Message, repo: Arc<dyn RudelistedRepository>) -> bool {
    if msg.id.0.wrapping_mul(32) % 81 > 65 {
        return false;
    }
    let Some(user) = msg.from() else {
        return false;
    };
    let chat_id = msg.chat.id;
    if let Err(err) = repo.clean_rudelisted().await {
        log::error!("Cannot clean rudelist\n{}", err);
        return false;
    }
    let rudelisted = match repo.list().await {
        Ok(rudelisted) => rudelisted,
        Err(err) => {
            log::error!("Cannot get rudelisted from db:\n{}", err);
            return false;
        }
    };
    crate::checks::is_rudelisted(user, chat_id, &rudelisted)
}

pub async fn process_member(msg: Message, repo: Arc<dyn MemberRepository>) -> bool {
    let Some(user) = msg.from() else {
        return true
    };
    let creds = Credentials::new(
        user.username.clone(),
        user.first_name.clone(),
        user.last_name.clone(),
    );
    let members = match repo.clean_members(&msg.chat.id).await {
        Ok(members) => members,
        Err(err) => {
            log::error!("{err:?}");
            return false;
        }
    };
    let credentials_equals = |m: &Member| m.creds.eq(&creds);
    let update_credentials =
        |m: &Member| Member::new(m.id, m.chat_id, creds.clone(), Some(msg.date));
    let current_member = members.iter().find(|m| m.id == user.id);
    let res = match current_member {
        None => {
            repo.add(match Member::from_message(&msg) {
                Some(member) => member,
                None => return false,
            })
            .await
        }
        Some(m) => {
            let mut m = m.clone();
            if !credentials_equals(&m) {
                m = update_credentials(&mut m);
            }
            repo.update(m).await
        }
    };
    if let Err(err) = res {
        log::error!("Unable to process member:\n{err:?}");
        return false;
    }
    true
}

pub fn kick_trigger(msg: Message, config: Arc<Config>) -> bool {
    if msg.forward().is_some() {
        return false;
    }
    for r in &config.regex.kick {
        if check_regex(&msg, r) {
            return true;
        }
    }
    false
}

pub fn is_command(msg: Message, config: Arc<Config>) -> Option<Arc<Command>> {
    let text = msg.text()?;
    for (k, v) in &config.commands {
        if v.is_match(text) {
            return Some(Arc::new(k.clone()));
        }
    }
    None
}

pub fn matches_counter(msg: Message, config: Arc<Config>) -> Option<Arc<(CounterEvent, Counter)>> {
    let text = msg.text()?;
    for c in &config.counters {
        if !c.matches_type(&msg) {
            continue;
        }
        for (e, r) in &c.regex {
            if r.is_match(text) {
                return Some(Arc::new((e.clone(), c.clone())));
            }
        }
    }
    None
}

fn check_regex(msg: &Message, regex: &Regex) -> bool {
    match msg.text() {
        Some(text) => regex.is_match(text),
        None => false,
    }
}
